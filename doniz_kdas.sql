-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 07, 2014 at 01:26 PM
-- Server version: 5.1.63
-- PHP Version: 5.3.19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `doniz_kdas`
--

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `members` (
  `uid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Vartotojo ID',
  `username` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL COMMENT 'Sukuriamas slapyvardis',
  `firstname` varchar(125) COLLATE utf8_lithuanian_ci NOT NULL COMMENT 'Vardas',
  `lastname` varchar(125) COLLATE utf8_lithuanian_ci NOT NULL COMMENT 'Pavardė',
  `phone` varchar(30) COLLATE utf8_lithuanian_ci NOT NULL COMMENT 'Telefono numeris',
  `address` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL COMMENT 'Adresas',
  `email` varchar(254) COLLATE utf8_lithuanian_ci NOT NULL COMMENT 'El. Pašto adresas',
  `password` varchar(100) COLLATE utf8_lithuanian_ci NOT NULL COMMENT 'Slaptažodis (Koduotė priklauso nuo sistemos)',
  `activate` varchar(30) COLLATE utf8_lithuanian_ci NOT NULL COMMENT 'Slaptažodis paskyros aktivacijai',
  `salt` varchar(15) COLLATE utf8_lithuanian_ci NOT NULL COMMENT 'Slaptažodio papildymas',
  `status` enum('inactive','active','deleted') COLLATE utf8_lithuanian_ci NOT NULL DEFAULT 'inactive' COMMENT 'Statusas',
  `created` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Registruota diena',
  `rid` int(11) NOT NULL DEFAULT '4' COMMENT 'Vartotojo teisės',
  PRIMARY KEY (`uid`),
  KEY `rid` (`rid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`uid`, `username`, `firstname`, `lastname`, `phone`, `address`, `email`, `password`, `activate`, `salt`, `status`, `created`, `rid`) VALUES
(1, 'donatas.navidonskis', 'Donatas', 'Navidonskis', '867078495', 'Vilnius, Saulėtekio 41-9', 'master@doniz.net', '12uSJSoRlR63U$dnNCHSWaiyC56llE6Dqa3zYLATAVfGrsxxge8uJyhy8xMF.ySdoZ7xOZoqC6jDgPmgcONROzr7mKRFCyAfJQA/', 'nE3dJYJ2B7RY58p3h1aMLa15quEeas', 'hbpYM5C3O4J5Z8H', 'active', 1354117680, 5),
(2, 'antanas.administratorius', 'Antanas', 'Administratorius', '867276367', 'Šiauliai, Tilžės g. 104', 'admin@doniz.net', '19359XScBjOgc$OM9cBY4YaWigNpBYZICeKCk66z6NEgXZiX.8jzPllfgwghGFOmBJ82LaAuo9l.wcq3x8Uo22eepoRTnLXvwaC.', 'YW6P4B9ZD8n8UFhFsqZ1uS3ny3LxQ8', 'dmW8w9mMqjt435V', 'active', 1354668360, 1),
(3, 'jonas.vedėjas', 'Jonas', 'Vedėjas', '861234567', 'Šiauliai, P. Višinskio g. 19', 'manager@doniz.net', '16G7pMj2gTvWI$NT1.4Kl/Rx0NLARDMom3f/M7R6EX9wyZFEuogB4ZHVPhc0F1C5a8vDnX0qIg04MR1UJflUUDJmIU1nCHnl6mm.', '94E0Xg0S6E4V3vt4doosfAlS3m7TGy', '35ct34B5L21Wy4t', 'active', 1354668420, 3),
(4, 'vytautas.dėstytojas', 'Vytautas', 'Dėstytojas', '861234567', 'Šiauliai, P. Višinskio g. 19', 'lecture@doniz.net', '10lM6MK3CgpOc$goLwu1JhOaSumEnh0/OW2VOeoyf8uuTxz8/RL4WkeVBFLBM6XmJ.kjuD0UEZrxHZCBO2LSqPuV/6L0joFJi8g/', 'MPYMzhS3dlPM6160vgVj3M36PT488g', '17h25OPEA17KMOe', 'active', 1354668420, 2),
(5, 'vytaras.studentas', 'Vytaras', 'Studentas', '861234567', 'Šiauliai, P. Višinskio g. 19', 'student@doniz.net', '14YGbKoGyAL9A$eH0eCoZXsIUWuEYtJK7JR3AG2.akYMoNISyUNgw8/4IQUgFIbqvG5tdWqIVLXMK0Fr6Tubq0cSE6jGZPk1Nma1', 'gXbpdmgU194hbI4Cu0mpmwIVjZu5qx', 'skVUYpt6OYIMho5', 'active', 1354668480, 4),
(7, 'jonas.jonaitis', 'Jonas', 'Jonaitis', '861234567', 'Šiauliai, P. Višinskio g. 19', 'manager2@doniz.net', '19359XScBjOgc$l1Axvt9u/zAL3RJz4qCCO70st6ByCzV8j8sPMglWfKT8hK1vZoO6U25yDe4.wJApOLmZ8MmW29f7VfrI2kYov0', '1C251fQ7DBl4pGb5466Y4uCMfgzy1H', 'yStjn4EQHbE6Z54', 'active', 1355446140, 3),
(8, 'petras.petraitis', 'Petras', 'Petraitis', '', '', 'lecture2@doniz.net', '2189FL3O8FDPk$.OhkSP5fvuz7ZBcDc6SNbGtKaWHU3RiwRi/3gk9UwqaaQmsd/jWBoalJ/AkdQe0l4Me64Ym3yYu6icGtStioT1', '2eH3gkA4bBgoI5JY8Dlzh1gk549qTd', '27445yP46le1Xw4', 'active', 1355446200, 2),
(9, 'antanas.antanaitis', 'Antanas', 'Antanaitis', '', '', 'lecture3@doniz.net', '41A7ubM5FdWpc$3vpBDhDQcLb/HfB.ewySEogPRFircGyftl1MU.AawXpQwoKH8OxeOmnQCJ161LgLOfefB62s3zMGBKOtCJgEP/', '9aHnWl1nnEQV53F5TmPwNc43BKgUh4', 'p7m4E9S5v590tkl', 'active', 1355459820, 2),
(10, 'vytenis.vytukas', 'Vytenis', 'Vytukas', '', '', 'lecture4@doniz.net', '903l7bubCac5s$QbrrDUO8.B5JOlduGWiWifmmYzWwQDVRloE2xopt6K/i1raJif2VsgHTpAfWAUcGOWlML.XPwdIQQ/rKBLopo0', 'G5No5pA1eLPkG8zQ5hxah1qrsJ958i', 'gsQzEnnyDOOpvwj', 'active', 1355459880, 2),
(11, 'kazys.kazlauskas', 'Kazys', 'Kazlauskas', '863459183', 'Vilnius, Rotušės g. 164', 'student2@doniz.net', '88IFMHp9xpRns$LoBkaZzHQPL/j7LD46FPirirmWbZZ48hT.hBbvyhTrXcrsMayYnT6g/IQ3DRT4P1/OdRsRV6Qqq6sj9mMdeJ00', 'kG8337XVcsoB8j4mtduAm4FI0IRqA8', '3uR53iPLPBI5lib', 'active', 1355481300, 4),
(12, 'donatas.navidonskis1', 'Donatas', 'Navidonskis', '862745928', '', 'donatas.navidonskis@gmail.com', '81DGNQDl3RR/o$nPey6v4Yot0nVS73JSGx4wR8etX2k2IghQ0/bjenPdqvMUrA19TDXG3/4e/tNUMHk6VqxkqMDVOijc9BKBERa1', 'fqAEU55s3id9OuRu2wSuy5AH5shAVu', 'Mzmfq14do1q88Yg', 'active', 1355680980, 4);

-- --------------------------------------------------------

--
-- Table structure for table `members_settings`
--

CREATE TABLE IF NOT EXISTS `members_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `start_date` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Diena nuo kada gali registruotis vartotojai',
  `end_date` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Diena iki kada gali registruotis vartotojai',
  `key` varchar(10) COLLATE utf8_lithuanian_ci NOT NULL COMMENT 'Registracijos raktas',
  `registrated_members` int(11) NOT NULL DEFAULT '0' COMMENT 'Kiek vartotoju uzsiregistravo per nustatyta data',
  `rid` int(11) NOT NULL COMMENT 'Vartotoju privilegijos / teises',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `members_settings`
--

INSERT INTO `members_settings` (`id`, `start_date`, `end_date`, `key`, `registrated_members`, `rid`) VALUES
(1, 1355637674, 1355724074, '8J0lO40OWx', 1, 4),
(2, 1363323101, 1364446301, '5FOXNIUN73', 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `pid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Permissions ID',
  `rid` varchar(30) COLLATE utf8_lithuanian_ci NOT NULL COMMENT 'Rank ID Name',
  `url` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL COMMENT 'Permissions Page Authenticate',
  `link_name` varchar(30) COLLATE utf8_lithuanian_ci NOT NULL COMMENT 'Migtuko vardas',
  `sub_url` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL COMMENT 'Sub kategorijos nuoroda',
  `icon_name` varchar(15) COLLATE utf8_lithuanian_ci NOT NULL COMMENT 'ikoneles vardas',
  `category` enum('puslapiai','vartotojai') COLLATE utf8_lithuanian_ci NOT NULL DEFAULT 'puslapiai',
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`pid`, `rid`, `url`, `link_name`, `sub_url`, `icon_name`, `category`) VALUES
(1, '1,2,3,5', 'auth', 'Informacija', '', 'icon-info-sign', 'puslapiai'),
(2, '5', 'auth/permission', 'Autorizacijos', '', 'icon-check', 'puslapiai'),
(3, '5', 'auth/addAccount', 'Pridėti paskyrą', NULL, 'icon-plus', 'vartotojai'),
(4, '4', 'main', 'Pagrindinis', '', 'icon-home', 'puslapiai'),
(6, '1,2,3,4,5', 'main/profile', 'Mano profilis', '', 'icon-user', 'vartotojai'),
(7, '2,5', 'subject', 'Tematika', '', 'icon-pencil', 'puslapiai'),
(8, '2,5', 'subject/add', 'Pridėti tematiką', 'subject', 'icon-plus', 'puslapiai'),
(9, '5', 'auth/permission/add', 'Pridėti naują', 'auth/permission', 'icon-plus', 'puslapiai'),
(10, '5', 'auth/permission/index', 'Sąrašas', 'auth/permission', 'icon-list', 'puslapiai'),
(11, '1,2,3,4,5', 'main/signout', 'Atsijungti', '', 'icon-off', 'vartotojai'),
(12, '2,5', 'subject/index', 'Sąrašas', 'subject', 'icon-list', 'puslapiai'),
(13, '2,5', 'subject/edit', 'Redaguoti', 'subject', 'icon-edit', 'puslapiai'),
(14, '2,5', 'subject/delete', 'Trinti', 'subject', 'icon-trash', 'puslapiai'),
(15, '1,3,5', 'job', 'Temų kiekis', '', 'icon-tasks', 'puslapiai'),
(16, '1,3,5', 'job/index', 'Sąrašas', 'job', 'icon-list', 'puslapiai'),
(17, '3,5', 'job/add', 'Pridėti', 'job', 'icon-plus', 'puslapiai'),
(18, '1,3,5', 'job/edit', 'Redaguoti', 'job', 'icon-edit', 'puslapiai'),
(19, '3,5', 'job/delete', 'Trinti', 'job', 'icon-trash', 'puslapiai'),
(20, '1,5', 'subject/approve', 'Patvirtinti Temas', '', 'icon-ok', 'puslapiai'),
(21, '1,5', 'accounts', 'Nustatymai', '', 'icon-wrench', 'vartotojai'),
(23, '1,5', 'accounts/settings', 'Pridėti naują', 'accounts', 'icon-plus', 'vartotojai'),
(24, '1,5', 'accounts/index', 'Sąrašas', 'accounts', 'icon-list', 'vartotojai');

-- --------------------------------------------------------

--
-- Table structure for table `rank`
--

CREATE TABLE IF NOT EXISTS `rank` (
  `rid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Rank ID',
  `name` varchar(30) COLLATE utf8_lithuanian_ci NOT NULL COMMENT 'Rank Name',
  `fullname` varchar(30) COLLATE utf8_lithuanian_ci NOT NULL COMMENT 'Rank Full Name',
  `description` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL,
  PRIMARY KEY (`rid`),
  KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci COMMENT='Members rules by rank' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `rank`
--

INSERT INTO `rank` (`rid`, `name`, `fullname`, `description`) VALUES
(1, 'admin', 'Administratorius (-ė)', 'Galite keisti užimtų / laisvų temų kiekį. Patvirtinti temas'),
(2, 'teacher', 'Dėstytojas (-a)', 'Galite užpildyti tematiką, darbo apibūdinimą / redaguoti'),
(3, 'manager', 'Vedėjas (-a)', 'Galite suvesti paskirtų darbų kiekį / redaguoti'),
(4, 'student', 'Studentas (-ė)', 'Galite peržiūrėti patvirtintą jūsų kursinio temą'),
(5, 'webmaster', 'Web Master', 'Visos svetainės teisės');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` varchar(40) COLLATE utf8_lithuanian_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(45) COLLATE utf8_lithuanian_ci NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_lithuanian_ci NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_lithuanian_ci NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('620716573a3e477e0148baf87aca5a63', '158.129.214.227', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36', 1399457022, 'a:4:{s:9:"user_data";s:0:"";s:7:"account";s:16:"master@doniz.net";s:4:"rank";s:1:"5";s:3:"uid";s:1:"1";}'),
('cc5b9dedab1a078ea0c757753ead4fb9', '78.56.98.200', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36', 1399454797, 'a:4:{s:9:"user_data";s:0:"";s:7:"account";s:16:"master@doniz.net";s:4:"rank";s:1:"5";s:3:"uid";s:1:"1";}');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE IF NOT EXISTS `subjects` (
  `sid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Temos ID',
  `title` varchar(255) COLLATE utf8_lithuanian_ci NOT NULL COMMENT 'Pavadinimas',
  `description` text COLLATE utf8_lithuanian_ci NOT NULL COMMENT 'Aprašymas',
  `lecture_id` int(11) NOT NULL COMMENT 'Dėstytojo ID',
  `student_id` int(11) NOT NULL COMMENT 'Studento ID arba NULL',
  `busy` enum('true','false') COLLATE utf8_lithuanian_ci NOT NULL DEFAULT 'false' COMMENT 'Ar užimta ?',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `sa_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`sid`, `title`, `description`, `lecture_id`, `student_id`, `busy`, `date`, `sa_id`) VALUES
(1, 'Protege įskiepių kūrimo mechanizmų tyrimas', 'Bus nagrinėjama Protege API, apžvelgiami esami įskiepiai, kuriami eksperimentiniai įskiepiai darbui su ontologijos redagavimu/atvaizdavimu.', 4, 0, 'false', 1355681400, 1),
(2, 'Kompiuterių tinklo modeliavimo galimybių su VMware emuliatoriumi tyrimas', 'Naudojant VMware modeliavimo programą ištirti tinklų modeliavimo galimybes:\n<ul>\n<li> LAN </li>\n<li>Korporatyvinis LAN su padaliniais, kurie apjungti saugiais tuneliais</li></ul>', 4, 5, 'true', 1355604720, 1),
(4, 'Kompiuterių tinklo modeliavimo galimybių su VirtualBox emuliatoriumi tyrimas', 'Naudojant VirtuaBox modeliavimo programą ištirti tinklų modeliavimo galimybes:\n<ul>\n<li>LAN</li>\n<li>Korporatyvinis LAN su padaliniais, kurie apjungti saugiais tuneliais</li>\n<li>Debesų technologinių sprendimų modelių kūrimų galimybes.</li></ul>', 8, 11, 'true', 1355682300, 2),
(5, 'Transporto eismo modeliavimo sistemų analizė ir taikymai', 'Susipažinti su įvairiomis transporto eismo modeliavimo sistemomis, išanalizuoti jų galimybes bei taikymus. Parinkti praktinių transporto valdymo ir eismo optimizacijos uždavinių, atlikti transporto eismo optimizacijos konkrečiose situacijose galimybių analizę ir modeliavimą su pasirinkta sistema.', 8, 12, 'true', 1382985900, 2);

-- --------------------------------------------------------

--
-- Table structure for table `subjects_amount`
--

CREATE TABLE IF NOT EXISTS `subjects_amount` (
  `sa_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Temu Kiekio ID',
  `lecture_id` int(11) NOT NULL,
  `amount` int(2) NOT NULL,
  `closed` enum('true','false') COLLATE utf8_lithuanian_ci NOT NULL DEFAULT 'false' COMMENT 'Jei kursinių darbų temas reikia uždaryti',
  PRIMARY KEY (`sa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci COMMENT='Temų kiekis' AUTO_INCREMENT=5 ;

--
-- Dumping data for table `subjects_amount`
--

INSERT INTO `subjects_amount` (`sa_id`, `lecture_id`, `amount`, `closed`) VALUES
(1, 4, 3, 'false'),
(2, 8, 2, 'false'),
(3, 9, 5, 'false'),
(4, 10, 10, 'false');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `members`
--
ALTER TABLE `members`
  ADD CONSTRAINT `members_ibfk_1` FOREIGN KEY (`rid`) REFERENCES `rank` (`rid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
