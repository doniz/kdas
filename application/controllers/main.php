<?php

class Main extends CI_Controller {

	private $sess_data = array();
	protected $data = array();

	public function __construct(){
		parent::__construct();

		$this->load->model('account');
		$this->load->library('password');
		$this->load->library('session');
		$this->load->library('encrypt');
		$this->load->model('permission');
		$this->load->model("subjects");

		$this->sess_data = array(
			'email' => $this->session->userdata('account'),
			'uid'	=> $this->session->userdata('uid'),
			'rid'	=> $this->session->userdata('rank')
		);

		$this->data['navigation'] = $this->permission->create_navigation(
			$this->sess_data
		);

	}

	public function icons(){

		$this->load->view('main/icons');

	}

	public function index(){

		if ( !$this->account->_is_logged($this->sess_data) ) {

			$cookie = get_cookie('syscookie');

			$cookie = preg_split('/:/', $this->encrypt->decode($cookie));

			if ( isset($cookie) && array_key_exists(0, $cookie) && array_key_exists(1, $cookie)) {
				// bandom prisijungti
				if ( $this->account->login(
						array(
							'email' 	=> $cookie[0],
							'password'	=> $cookie[1],
							'remember'	=> false
						)
					)
				)
					redirect('auth');
			}

			$this->data['content'] = 'main/sign-in';


		} else {

			if( $this->account->secure(array('rid' => 4)) ){
			
				$this->data["subjects"] = $this->subjects->get(array("busy" => "false", "student_id" => 0));
				$this->data["subject"]  = $this->subjects->get(array("student_id" => $this->sess_data["uid"]));
			
			}

			$this->data['content'] = 'main/home';

		}


		$this->load->view('template', $this->data);

	}

	public function select($id = ''){

		if ( !$this->account->_is_logged($this->sess_data) )
			redirect('main');

		// Tik studentai gali rinktis kursinio temas
		if( $this->account->secure(array('rid' => 4)) && !empty($id) ) {

			// tikrinti ar studentas nera pasirinkes jau temos
			$subject = $this->subjects->get(array("student_id" => $this->sess_data["uid"]));

			if(empty($subject)){

				if($this->subjects->update(array("sid" => $id, "student_id" => $this->sess_data["uid"])))
					redirect('main/index');

			}

		}

	}

	public function deselect($id = ''){

		if ( !$this->account->_is_logged($this->sess_data) )
			redirect('main');

		// Tik studentai gali rinktis kursinio temas
		if( $this->account->secure(array('rid' => 4)) && !empty($id) ) {

			// tikrinti ar studentas nera pasirinkes jau temos
			$subject = $this->subjects->get(array("student_id" => $this->sess_data["uid"]));

			if(!empty($subject)){

				if($this->subjects->update(array("sid" => $id, "student_id" => 0)))
					redirect('main/index');

			}

		}

	}

	public function profile($id = '', $password = ''){

		if ( !$this->account->_is_logged($this->sess_data) )
			redirect('main');

		$this->data["profile"] = $this->account->get(array("uid" => $this->sess_data["uid"]));
		$this->data["rank"]	   = $this->permission->get_rank(array("rid" => $this->data["profile"]->rid));

		if( empty($id) ){

			$this->data["content"] = "main/profile-view";

			$this->load->view("template", $this->data);

		} else {

			// Keisti slaptazodi
			if( !empty($password) && $password == "password" ){

				$this->form_validation->set_rules('password', 'Dabartinis slaptažodis', "trim|required|min_length[7]");
				$this->form_validation->set_rules('new_password', 'Naujas slaptažodis', "trim|required|matches[new_password_conf]|min_length[7]");
				$this->form_validation->set_rules('new_password_conf', 'Slaptažodžio patvirtinimas', 'trim|required');

				if( $this->form_validation->run() == false ) {

					$this->data["password_error"] = validation_errors();

					$this->data["content"] = "main/profile-edit";

					$this->load->view("template", $this->data);
				
				} else {

					if( !$this->password->check_password($this->data["profile"]->password, $this->input->post('password'), $this->data["profile"]->salt) ){
					
						$this->data["password_error"] = "Dabartinis slaptažodis neteisingas";

						$this->data["content"] = "main/profile-edit";

						$this->load->view("template", $this->data);

					} else {

						if( $this->account->update(array("uid" => $id, "password" => $this->input->post("new_password"))) ){

							$this->data["password_success"] = "Slaptažodis sėkmingai pakeistas !";

							$this->data["content"] = "main/profile-edit";

							$this->load->view("template", $this->data);

						}

					}

				} 

			// keisti kitus duomenis
			} else {

				if( $this->sess_data["uid"] == $id ){

					$this->form_validation->set_rules("firstname", "Vardas", "trim|required");
					$this->form_validation->set_rules("lastname", "Pavardė", "trim|required");	
					$this->form_validation->set_rules("phone", "Telefonas", "trim|integer");
					$this->form_validation->set_rules("address", "Adresas", "trim");

					if( $this->form_validation->run() == false ) {

						$this->data["error"] = validation_errors();

						$this->data["content"] = "main/profile-edit";

						$this->load->view("template", $this->data);

					} else {

						$data = array();

						$data["firstname"]  = $this->input->post("firstname");
						$data["lastname"]   = $this->input->post("lastname");
						$data["phone"]		= $this->input->post("phone");
						$data["address"]	= $this->input->post("address");
						$data["uid"]		= $id;

						if( $this->account->update($data) ) {

							$this->data["content_success"] = "Duomenys sėkmingai atnaujinti !";

							$this->data["content"] = "main/profile-edit";

							$this->load->view("template", $this->data);

						} else

							redirect('main/profile');

					}

				}

			}

		}

	}

	public function activate($hash = ''){
		if ( !empty($hash) ) {

			if( !$this->account->_is_logged($this->sess_data) ) {

				if ( $this->account->activate($hash) ) {

					$member = $this->account->get(
						array('activate' => $hash)
					);

					$this->data['email']	 = $member[0]->email;
					$this->data['content'] = 'account/activated';

					$this->load->view('template', $this->data);

				} else 

					redirect('main');

			} else {

				$this->account->logout();

				redirect('main/activate/' . $hash);
			}

		} else

			redirect('main');

	}

	/**
	 * @var $kind ( email, reset )
	 * @var $activate | hash password from email
	 *
	*/
	public function password($kind = 'email', $activate = ''){

		if ( $kind == 'email' ) {

			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|exist[members.email]');

			$this->data['content'] = 'account/email_auth';

			if ( $this->form_validation->run() == FALSE ) {

				$this->data['error'] = validation_errors();

				$this->load->view('template', $this->data);

			} else {

				$member = $this->account->get(
					array(
						'email' => $this->input->post('email')
					)
				);

				$activate = $this->password->_generate_salt(30);

				if( !$this->account->update(
						array(
							'uid' 		=> $member->uid,
							'activate'	=> $activate
						)
					)
				) {
					$this->data['error'] = 'Negalime pakeisti slaptažodžio. Prašome apie tai pranešti svetainės administratoriui info(at)doniz.net';
					$this->load->view('template', $this->data);
				}
				
				$this->load->library('email');

				$config['useragent'] = 'doniz.net';
				$config['protocol']	 = 'smtp';
				$config['smtp_host'] = 'su.lt';
				$config['smtp_user'] = 'programavimas@ik.su.lt';
				$config['smtp_pass'] = '';

				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				$config['priority'] = 1;

				$this->email->initialize($config);

				$this->email->from('programavimas@ik.su.lt', 'no-reply');
				$this->email->to($member->email);

				$this->email->subject('SU - Vartotojo aktyvacija');
				$this->email->message('Sveiki ' . $member->firstname . ',

					Jūsų aktyvacijos nuoroda: ' . site_url('main/password/reset/'.$activate) . ' 

				Jei apie tai nieko nežinote, ignoruokite šį laišką.');	

				$this->email->send();

				$this->data['content'] = 'account/email_auth_success';
				$this->load->view('template', $this->data);

			}

		} elseif ( $kind == 'reset' && !empty($activate) && strlen($activate) == 30 ) {

			// Activating account
			$this->account->activate($activate);

			$this->form_validation->set_rules('password', 'Password', "trim|required|matches[passwordConfirmation]|min_length[7]");
			$this->form_validation->set_rules('passwordConfirmation', 'Password Confirmation', 'trim|required');

			$this->data['action']	 = site_url('main/password/'.$kind.'/'.$activate);
			$this->data['content'] = 'account/reset_password';

			if ( $this->form_validation->run() == FALSE ) {

				$this->data['error'] = validation_errors();

				$this->load->view('template', $this->data);

			} else {

				$member = $this->account->get(
					array(
						'activate' => $activate
					)
				);


				if ( !$member[0] ) { 

					$this->data['error'] = 'Aktyvacijos kodas blogas.'; 

					$this->load->view('template', $this->data); exit; 

				}

				if ( !$this->account->update(
						array(
							'uid' => $member[0]->uid,
							'password' => $this->input->post('password')
						)
					)
				) {

					$this->data['error'] = 'Negalime pakeisti slaptažodžio. Prašome apie tai pranešti svetainės administratoriui info(at)doniz.net';

					$this->load->view('template', $this->data);

				} else {

					$data['content'] = 'account/reset_password_success';

					$this->load->view('template', $this->data);

				}

			}


		}

		
	}

	public function signout(){

		if ( $this->account->logout() )

			redirect('main');

	}

	public function signin(){

		if ( $this->account->_is_logged($this->sess_data) )

			redirect('auth');

		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|exist[members.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		$this->data['content'] = 'main/sign-in';

		if ( $this->form_validation->run() == FALSE ) {

			$this->data['error'] = validation_errors();

			$this->load->view('template', $this->data);

		} else {

			$member = array(

				'email'		=> $this->input->post('email'),
				'password'	=> $this->input->post('password'),
				'remember'	=> ( $this->input->post('remember') ) ? true : false
			
			);

			if ( $this->account->login($member) ) {

				redirect('auth');

			} else {

				$this->data['error'] = 'Blogi duomenys arba paskyra nėra aktyvuota';
				
				$this->load->view('template', $this->data);

			}
		}
	}
}