<?php

class Subject extends CI_Controller {

	protected $data		= array();

	public function __construct(){
		parent::__construct();

		$this->load->model('account');
		$this->load->model('permission');
		$this->load->model('subjects');

		$permission = $this->permission->get(array('url' => $this->permission->_uri(uri_string())));

		$require_secure = true;

		foreach($permission as $perm){

			if( $this->account->secure(array('rid' => $perm->rid)) )
				$require_secure = false;

		}

		if( $require_secure )

			redirect('main');


		$this->data['navigation'] = $this->permission->create_navigation(
			array(
				'email' => $this->session->userdata('account'),
				'rid' => $this->session->userdata('rank'),
				'uid' => $this->session->userdata('uid')
			)
		);

	}
                
	public function index(){

		$lecture = $this->account->get(array("email" => $this->session->userdata('account')));

		$this->data['amount']	= $this->subjects->get_amount(array('lecture_id' => $lecture->uid));

		if(isset($this->data['amount']->sa_id))

			$this->data['subjects']	= $this->subjects->get(array('lecture_id' => $lecture->uid, 'sort_by' => 'date', 'sort_direction' => 'DESC', 'sa_id' => $this->data['amount']->sa_id));
		else
			$this->data['subjects']	= $this->subjects->get(array('lecture_id' => $lecture->uid, 'sort_by' => 'date', 'sort_direction' => 'DESC'));

		$this->data['content'] 	= 'subject/view';

		$this->load->view('template', $this->data);

	}

	public function approve($action = '', $id = ''){


		if($action == 'edit' && !empty($id) && is_numeric($id)){

			// validation rules
			$this->form_validation->set_rules('title', 'Pavadinimas', 'trim|required');
			$this->form_validation->set_rules('description', 'Apibūdinimas', 'trim|required');

			if( $this->form_validation->run() == false ){

				$this->data['subject'] = $this->subjects->get(array('sid' => $id));

				$this->data['content'] = 'subject/approve-edit';

				$this->load->view('template', $this->data);

			} else {

				$post_data = $this->input->post();
				$post_data['sid'] = $id;
				unset($post_data['approve_edit']);

				$this->subjects->update($post_data);

				redirect('subject/approve');

			}

		} elseif($action == 'cancel' && !empty($id) && is_numeric($id)) {

			$this->subjects->update(array('sid' => $id, 'busy' => 'false'));
			
			redirect('subject/approve');


		} elseif($action == 'apply' && !empty($id) && is_numeric($id)) {

			$this->subjects->update(array('sid' => $id, 'busy' => 'true'));

			redirect('subject/approve');

		} else {

			// gauti visas destytoju paskyras
			$this->data['lectures']		= $this->account->get(array('rid' => 2));
			// gauti temu kiekius
			$this->data['amounts']		= $this->subjects->get_amount();
			// gauti nepatvirtintas temas
			$this->data['subjects']		= $this->subjects->get(array('sort_by' => 'date', 'sort_direction' => 'DESC', 'busy' => 'false'));
			// gauti tik patvirintas temas
			$this->data['subjects_app']	= $this->subjects->get(array('sort_by' => 'date', 'sort_direction' => 'DESC', 'busy' => 'true'));

			$this->data['content']		= 'subject/approve-list';

			$this->load->view('template', $this->data);

		}

	}

	public function delete($id = ''){

		if( empty($id) )
			redirect('subject/index');

		$lecture = $this->account->get(array("email" => $this->session->userdata('account')));

		$amount = $this->subjects->get_amount(array('lecture_id' => $lecture->uid));

		if(isset($amount->sa_id))
			$subject = $this->subjects->get(array('sid' => $id, 'lecture_id' => $lecture->uid, 'sa_id' => $amount->sa_id));
		else
			$subject = $this->subjects->get(array('sid' => $id, 'lecture_id' => $lecture->uid));

		// Tikrinti ar tema  uzdaryta, tuomet neleisti nieko daryti ---
			if( $this->subjects->is_closed($subject, $amount) || $this->subjects->is_busy($subject) )
				redirect('subject/index');
		// -----

		if($subject->busy == 'true'){
			
			redirect('subject/index');

			return false;

		}

		if( $this->subjects->delete(array("sid" => $id, 'lecture_id' => $lecture->uid)) )
			redirect('subject/index');
	}

	public function edit($id = ''){

		if( empty($id) )
			redirect('subject/index');


		$lecture = $this->account->get(array("email" => $this->session->userdata('account')));

		$this->data['subject'] = $this->subjects->get(array('lecture_id' => $lecture->uid, 'sid' => $id));

		$this->data['students'] = $this->account->get(array('rid' => '4'));

		$this->data['students'] = $this->subjects->_not_exist_students($this->data['students']);

		$amount = $this->subjects->get_amount(array('lecture_id' => $lecture->uid), TRUE);


		// Tikrinti ar tema  uzdaryta, tuomet neleisti nieko daryti ---
			if( $this->subjects->is_closed($this->data['subject'], $amount) || $this->subjects->is_busy($this->data['subject']) )
				redirect('subject/index');
		// -----

		$this->form_validation->set_rules('title', 'Pavadinimas', 'required|trim|max_length[255]');
		$this->form_validation->set_rules('description', 'Apibūdinimas', 'required|trim');
		$this->form_validation->set_rules('student_id', 'Studentas', 'trim');

		if( $this->form_validation->run() == FALSE ) {

			$this->data['error'] = validation_errors();

			$this->data['content'] = 'subject/edit';

			$this->load->view('template', $this->data);

		} else {

			$data = array(
					'sid'			=> $id,
					"title"			=> $this->input->post('title'),
					"description" 	=> $this->input->post('description'),
					"lecture_id"	=> $lecture->uid,
					"student_id"	=> $this->input->post('student_id')
				);

			if($this->subjects->update($data))

				redirect('subject/index');

		}

	}

	/**
	*	Duomenu bazes laukeliai:
	*	------------------------------------
	*	sid 		AUTO
	*	title 		string 				required
	*	description string 				required
	*	lecture_id	members->array() 	required
	*	student_id	members->array() 	optional
	*	busy 		true/false 			default: false optional
	*	date 		unix timestamp 		required
	*
	*	@add Naujos tematikos kurimas
	* --------------------------------------
	*	
	*	@param none
	*	@return none
	*
	*/
	public function add(){

		$member = $this->account->get(array("email" => $this->session->userdata('account')));
		$manager = $this->account->get(array('rid' => 3));

		$this->data['students'] = $this->subjects->_not_exist_students($this->account->get(array('rid' => '4')));
		$this->data['amount']	= $this->subjects->get_amount(array('lecture_id' => $member->uid));

		if(isset($this->data['amount']->sa_id))
			$this->data['subjects']	= $this->subjects->get(array('lecture_id' => $member->uid, 'sa_id' => $this->data['amount']->sa_id));
		else
			$this->data['subjects']	= $this->subjects->get(array('lecture_id' => $member->uid));

		$this->data['manager']	= $manager;

		$this->form_validation->set_rules('title', 'Pavadinimas', 'required|trim|max_length[255]');
		$this->form_validation->set_rules('description', 'Apibūdinimas', 'required|trim');
		$this->form_validation->set_rules('student_id', 'Studentas', 'trim');

		if( $this->form_validation->run() == FALSE ) {

			$this->data['error'] = validation_errors();

			$this->data['content'] = 'subject/add';

			$this->load->view('template', $this->data);

		} else {

			$data = array(
					"title"			=> $this->input->post('title'),
					"description" 	=> $this->input->post('description'),
					"lecture_id"	=> $member->uid,
					"student_id"	=> $this->input->post('student_id'),
					"student_empty"	=> $this->input->post('student_empty'),
					"sa_id"			=> $this->data['amount']->sa_id
				);

			$this->subjects->insert($data);

			$this->data['content'] = 'subject/add_success';

			$this->load->view('template', $this->data);

		}
	}

}