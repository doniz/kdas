<?php

class Auth extends CI_Controller {
	
	private $data = array();

	public function __construct(){
		parent::__construct();

		$this->load->model('account');
		$this->load->model('permission');
		$this->load->model('subjects');

		$permission = $this->permission->get(array('url' => $this->permission->_uri(uri_string())));

		$require_secure = true;

		foreach($permission as $perm){

			if( $this->account->secure(array('rid' => $perm->rid)) )
				$require_secure = false;

		}

		if( $require_secure )

			redirect('main');


		$this->data['navigation'] = $this->permission->create_navigation(
			array(
				'email' => $this->session->userdata('account'),
				'rid' => $this->session->userdata('rank'),
				'uid' => $this->session->userdata('uid')
			)
		);
		
	}

	public function __destruct(){

		unset($this->data);
	
	}

	/*
		@rangas
		
		rid 	fullname
		------+-----------------------+
			1	Administratorius (-ė)
			2	Dėstytojas (-a)
			3	Vedėjas (-a)
			4	Studentas (-ė)
			5	Web Master
	*/
	public function index(){

		$users = $this->account->get();
		$this->data['lectures'] = $this->account->get(array('rid' => 2));


		$members = array(
			1 => array(
				"rid" => 1,
				"count" => 0,
				"active" => 0,
				"inactive" => 0
			),
			2 => array(
				"rid" => 2,
				"count" => 0,
				"active" => 0,
				"inactive" => 0
			),
			3 => array(
				"rid" => 3,
				"count" => 0,
				"active" => 0,
				"inactive" => 0
			),
			4 => array(
				"rid" => 4,
				"count" => 0,
				"active" => 0,
				"inactive" => 0
			),
			5 => array(
				"rid" => 5,
				"count" => 0,
				"active" => 0,
				"inactive" => 0
			),

		);

		foreach($users as $u){

			$index = 1;

			foreach($members as $m){

				if($u->rid == $index){

					$members[$index]["count"]++;

					if($u->status == 'active')
						$members[$index]["active"]++;

					if($u->status == 'inactive')
						$members[$index]["inactive"]++;

				}

				$index++;

			}

		}

		$this->data["members"] = $members;



		// gauti temu kiekius
		$this->data['amount']		= $this->subjects->get_amount(array(), TRUE);
		// gauti nepatvirtintas temas
		$this->data['subjects']		= $this->subjects->get(array('sort_by' => 'date', 'sort_direction' => 'DESC'));



		$this->data['content'] = 'auth/index';

		$this->load->view('template', $this->data);

	}

	public function signout(){
		if ( $this->account->logout() )
			redirect('main');
	}

	public function permission($action = '', $id = ''){
		$action = strtolower($action);

		$this->form_validation->set_rules('rid[]', 'Teisės', 'trim|required');
		$this->form_validation->set_rules('url', 'URL', 'required|trim');
		$this->form_validation->set_rules('link_name', 'Pavadinimas', 'required|trim');
		$this->form_validation->set_rules('sub_url', 'Pakategorės', 'trim');
		$this->form_validation->set_rules('icon_name', 'Įkonėlė', 'trim');
		$this->form_validation->set_rules('category', 'Kategorija', 'required|trim');

		switch($action){

			case 'add':
				

				$this->data['rid']		= $this->permission->get_rank();
				$this->data['content'] 	= 'permission/add';

				if( $this->form_validation->run() == FALSE ) {

					$this->data['error'] = validation_errors();

					$this->load->view('template', $this->data);

				} else {

					$options = array();

					if($this->input->post("rid") != "")
						$options["rid"] = $this->input->post("rid");

					if($this->input->post("url") != "")
						$options["url"] = $this->input->post("url");

					if($this->input->post("link_name") != "")
						$options["link_name"] = $this->input->post("link_name");

					if($this->input->post("sub_url") != "")
						$options["sub_url"] = $this->input->post("sub_url");

					if($this->input->post("icon_name") != "")
						$options["icon_name"] = $this->input->post("icon_name");

					$options["category"] = $this->input->post("category");


					if($this->permission->add($options)) {

							$this->data['content'] = 'permission/add_success';

					} else {

							$this->data['error'] = 'Can\' add your record.';

					}

						$this->load->view('template', $this->data);

				}

			break;

			case 'edit':

				// Rodyti teisiu saraso lentele
				if( !empty($id) && is_numeric($id) ) {

					$this->data['permissions'] = $this->permission->get(array('pid' => $id));
					$this->data['rid'] = $this->permission->get_rank();
					$this->data['pid'] = $id;
					$this->data['content']	 = 'permission/edit';


					if( $this->form_validation->run() == FALSE ) {

						$this->data['error'] = validation_errors();

						

						$this->load->view('template', $this->data);

					} else {

						$options = array();

						$options['pid'] = $id;

						if($this->input->post("rid") != "")
							$options["rid"] = $this->input->post("rid");

						if($this->input->post("url") != "")
							$options["url"] = $this->input->post("url");

						if($this->input->post("link_name") != "")
							$options["link_name"] = $this->input->post("link_name");

						$options["sub_url"] = $this->input->post("sub_url");
						
						$options["icon_name"] = $this->input->post("icon_name");

						$options["category"] = $this->input->post("category");

						if($this->input->post("read") != "")
							$options["read"] = $this->input->post("read");

						if($this->input->post("edit") != "")
							$options["edit"] = $this->input->post("edit");


						$this->permission->update($options);

						redirect('auth/permission');

					}

				}

			break;

			case 'delete':

				// Rodyti teisiu saraso lentele
				if( !empty($id) && is_numeric($id) ) {

					if( $this->permission->delete($id) )

						redirect('auth/permission');

				}

			break;

			// view
			default:

				$this->data['permissions'] = $this->permission->get();
				$this->data['content']	 = 'permission/view';

				$this->load->view('template', $this->data);

			break;

		}
	}

	public function addAccount(){

		$this->form_validation->set_rules("email", "El. Paštas", "trim|required|valid_email|is_unique[members.email]");
		$this->form_validation->set_rules("password", "Slaptažodis", "trim|required|matches[passwordConfirmation]|min_length[7]");
		$this->form_validation->set_rules("passwordConfirmation", "Slaptažodžio patvirtinimas", "trim|required");
		$this->form_validation->set_rules("firstname", "Vardas", "trim|required");
		$this->form_validation->set_rules("lastname", "Pavardė", "trim|required");

		if($this->form_validation->run() == FALSE) {

			$this->data['error'] 		= validation_errors();
			$this->data['content'] 		= 'account/signup';
			$this->data['ranks']		= $this->account->get_rank();
			
			$this->load->view("template", $this->data);

		} else {

			$options = array();
				if($this->input->post("firstname") != "")
					$options["firstname"] = $this->input->post("firstname");
				if($this->input->post("lastname") != "")
					$options["lastname"] = $this->input->post("lastname");
				if($this->input->post("email") != "")
					$options["email"] = $this->input->post("email");
				if($this->input->post("password") != "")
					$options["password"] = $this->input->post("password");
				if($this->input->post("status") != "")
					$options["status"] = $this->input->post("status");
				if($this->input->post("rid") != "")
					$options["rid"] = $this->input->post("rid");
				if($this->input->post("created") != "")
					$options["created"] = $this->input->post("created");

								
			$this->account->add($options);
			$this->data['content'] = 'account/signup_success';
			$this->load->view("template", $this->data);
		}
	}
}