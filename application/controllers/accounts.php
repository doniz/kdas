<?php

class Accounts extends CI_Controller {

	protected $data = array();
	
	/*
	if($require_secure == true){
		
		if(uri_string() == 'accounts/validate')
			$require_secure = false;

		if($this->settings->is_match_key($this->session->userdata('key')) && uri_string() == 'accounts/signup')
			$require_secure = false;

	}
	*/

	public function __construct(){
		parent::__construct();

		$this->load->model('account');
		$this->load->model('permission');
		$this->load->library('password');
		$this->load->model('settings');

		$permission = $this->permission->get(array('url' => $this->permission->_uri(uri_string())));

		$require_secure = TRUE;

		foreach($permission as $perm){

			if( $this->account->secure(array('rid' => $perm->rid)) )
				$require_secure = FALSE;
			
		}
		
		if($require_secure == true){
			
			if(uri_string() == 'accounts/validate')
				$require_secure = false;

			if($this->settings->is_match_key($this->session->userdata('key')) && uri_string() == 'accounts/signup')
				$require_secure = false;

		}
		
		if( $require_secure )

			redirect('auth');


		$this->data['navigation'] = $this->permission->create_navigation(
			array(
				'email' => $this->session->userdata('account'),
				'rid' => $this->session->userdata('rank'),
				'uid' => $this->session->userdata('uid')
			)
		);
		
	}

	// Nustatymai
	// nustatyti leidimus, kas tures teise pagal ranga registruotis sugeneruotu sistemos kodu
	public function index(){

		$this->data['settings'] = $this->settings->get();
		$this->data['ranks']	= $this->permission->get_rank();
		$this->data['content']  = 'account/settings';

		$this->load->view('template', $this->data);
		


	}

	public function settings($action = 'add', $id = ''){

		$this->data['settings'] = $this->settings->get();

		if($action == 'add'){

			$this->form_validation->set_rules('end_date', 'Diena', 'required');
			$this->form_validation->set_rules('rid', 'Teisės', 'required');

			if( $this->form_validation->run() == false ){

				$this->data['ranks'] 	= $this->permission->get_rank();
				$this->data['content']	= 'account/settings';
				$this->data['error']	= validation_errors();

				$this->load->view('template', $this->data);

			} else {

				$options['start_date'] 	= human_to_unix(date("Y-m-d h:i:s", time()));
				$options['end_date']	= human_to_unix($this->input->post('end_date') . " " . date("h:i:s", time()));
				$options['rid']			= $this->input->post('rid');
				$options['key'] 		= $this->password->_generate_salt(10);

				$this->settings->insert($options);

				redirect('accounts/index');

			}

		} elseif($action == 'remove' && !empty($id) && is_numeric($id) ){
		
			$this->settings->delete(array('id' => $id));
			
			redirect('accounts/settings');
		
		}
	}

	public function validate(){

		$this->session->unset_userdata('key');
		
		$key 	= $this->input->post('key');
		$result = $this->settings->is_match_key($key);

		if( $result == 1 )
			$this->session->set_userdata('key', $key);

		echo $result;
	}

	public function signup(){

		$key 	= $this->session->userdata('key');
		$result = $this->settings->is_match_key($key);

		if( $result == 1 ){

			$this->form_validation->set_rules("email", "El. Paštas", "trim|required|valid_email|is_unique[members.email]");
			$this->form_validation->set_rules("password", "Slaptažodis", "trim|required|matches[passwordConfirmation]|min_length[7]");
			$this->form_validation->set_rules("passwordConfirmation", "Slaptažodžio patvirtinimas", "trim|required");
			$this->form_validation->set_rules("firstname", "Vardas", "trim|required");
			$this->form_validation->set_rules("lastname", "Pavardė", "trim|required");
			$this->form_validation->set_rules("phone", "Telefonas", "trim|required|integer");
			$this->form_validation->set_rules("address", "Adresas", "trim");


			if( $this->form_validation->run() == false ){

				$this->data['error'] 	= validation_errors();
				$this->data['content']  = 'account/signup-for-public';
				$this->data['rank']		= $this->permission->get_rank();

				$this->load->view('template', $this->data);

			} else {

				$settings = $this->settings->get(array('key' => $this->session->userdata('key')));

				$options = $this->input->post();
				$options['rid'] = $settings->rid;
				unset($options['passwordConfirmation']);

				$this->account->add($options);

				$this->data['content']	= 'account/signup-success-public';

				// atnaujinti duomenu lentele, kad pridetas naujas vartotojas
				$this->settings->update(array('id' => $settings->id, 'registrated_members' => $settings->registrated_members + 1));
				
				$new_member = $this->account->get(array('email' => $options['email']));

				$this->load->library('email');

				$config['useragent'] = 'doniz.net';
				$config['protocol']	 = 'smtp';
				$config['smtp_host'] = 'su.lt';
				$config['smtp_user'] = 'programavimas@ik.su.lt';
				$config['smtp_pass'] = '';

				$config['charset'] = 'iso-8859-1';
				$config['wordwrap'] = TRUE;
				$config['priority'] = 1;

				$this->email->initialize($config);

				$this->email->from('programavimas@ik.su.lt', 'no-reply');
				$this->email->to($new_member->email);

				$this->email->subject('SU - Vartotojo aktyvacija');
				$this->email->message('Sveiki ' . $new_member->firstname . ',

					Jūsų aktyvacijos nuoroda: ' . site_url('main/password/reset/'.$new_member->activate) . ' 

				Jei apie tai nieko nežinote, ignoruokite šį laišką.');	

				$this->email->send();
				
				// isregistruoti rakta
				$this->session->unset_userdata('key');

				$this->load->view('template', $this->data);

			}

		} else

			redirect('main');


	}

}