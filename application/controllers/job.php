<?php

class Job extends CI_Controller {

	protected $data = array();

	public function __construct(){
		parent::__construct();

		$this->load->model('account');
		$this->load->model('permission');
		$this->load->model('subjects');
		$this->load->model('job_model');

		$permission = $this->permission->get(array('url' => $this->permission->_uri(uri_string())));

		$require_secure = true;

		foreach($permission as $perm){

			if( $this->account->secure(array('rid' => $perm->rid)) )
				$require_secure = false;
			else
				$this->data["requried_secure"] = true;

		}

		if( $require_secure )

			redirect('main');


		$this->data['navigation'] = $this->permission->create_navigation(
			array(
				'email' => $this->session->userdata('account'),
				'rid' => $this->session->userdata('rank'),
				'uid' => $this->session->userdata('uid')
			)
		);

	}

	public function index(){

		$this->data['lectures'] = $this->account->get(array('rid' => 2));

		$this->data['amount']	= $this->job_model->get();

		$this->data['subjects'] = $this->subjects->get();

		$this->data['content'] 	= 'job/view';

		$this->load->view('template', $this->data);
		
	}

	public function delete($id = ''){

		if( !empty($id) ){

			$subject = $this->subjects->get(array("sa_id" => $id));

			if(is_array($subject)){
				
				foreach($subject as $s){

					$this->subjects->delete(array("sid" => $s->sid, "lecture_id" => $s->lecture_id));
				
				}

			}

			$this->job_model->delete(array("sa_id" => $id));

		}

		redirect('job/index');
	}

	public function edit($id = ''){

		if( empty($id) )
		
			redirect('job/index');

		else {

			$this->data["amount"] = $this->job_model->get(array("sa_id" => $id));
			$this->data["lecture"] = $this->account->get(array("uid" => $this->data["amount"]->lecture_id));
			$subjects = $this->subjects->get(array("sa_id" => $this->data["amount"]->sa_id));

			$this->form_validation->set_rules('amount', 'Temų kiekis', 'trim|required|integer');
			$this->form_validation->set_rules('lecture_id', 'Dėstytojas', 'trim|required|integer');

			if( $this->form_validation->run() == FALSE ) {

				$this->data["error"] = validation_errors();

				$this->data["content"] = "job/edit";

				$this->load->view("template", $this->data);

			} else {

				$amount = $this->input->post('amount');
				$l_id 	= $this->input->post('lecture_id');
				$delete = array();

				if($amount == 0)

					$amount = 1;

				if( ($this->data['amount']->amount > $amount) )  {

					$count = 0;

					foreach($subjects as $s)
						if($s->busy == "true")
							$count++;


					for($i = $this->data['amount']->amount; $i > $amount; $i--){

						foreach($subjects as $s){

							if($s->busy == "false" && $i <= count($subjects)){

								$delete[] = $s->sid;

								break;

							}

						}

						if( $i - 1 == 0 ) {

							$amount = 1;
							break;

						}

					}

				if($count >= $amount)
					$amount = $count;
				
				}

				if(count($delete) > 0){

					foreach($delete as $d){

						$this->subjects->delete(array("sid" => $d, "lecture_id" => $l_id));

					}
				}

				$this->job_model->update(array("sa_id" => $id, "amount" => $amount, "lecture_id" => $l_id));
				
				redirect('job/index');

			}

		}
	}

	public function add($id = ''){

		$this->form_validation->set_rules('amount', 'Temų kiekis', 'trim|required|integer');
		$this->form_validation->set_rules('lecture_id', 'Dėstytojas', 'trim|required|integer');

		$is_ok = false;


		if( !empty($id) ) {

			if( $this->form_validation->run() == FALSE ){
				// gauname duomenis per vartotojo id
				$member = $this->account->get(array('uid' => $id));
				// tirkinti ar tai destytojas
				if( $member->rid == 2 ) {
					// tikrinti ar sitam destytojui jau nera priskirtas darbo kiekis ir ar temos nera uzdarytos
					$subject = $this->subjects->get_amount(array('lecture_id' => $member->uid));
					// jei $subjects yra duomenis, tuomet grazinti taip pat i job/index nes sis metodas 'add' tik ikelia naujus duomenis
					// o ne radaguoja
					//print_r($subject);

					if( isset($subject->sa_id))

						redirect('job/index');
					
					else {

						$this->data['lecture'] = $member;

						$this->data['content'] = 'job/add';

						$this->data['error'] = validation_errors();

						$this->load->view('template', $this->data);

					}

				} else
					// jei ne tuomet grizti
					redirect('job/index');
			
			} else

				$is_ok = true; 


		} else {


			if( $this->form_validation->run() == FALSE ) {

				$this->data['content'] = 'job/add';

				$this->data['error'] = validation_errors();

				$this->data['lectures'] = $this->account->get(array('rid' => 2));

				$this->data['lectures'] = $this->job_model->_not_exist_lectures($this->data['lectures']);

				$this->load->view('template', $this->data);

			} else
				$is_ok = true;

		}

		// jei true, tuomet kelti duomenis
		if( $is_ok ) {

			$data = array(
				"lecture_id" => $this->input->post("lecture_id"),
				"amount"	 => $this->input->post("amount"),
				"closed"	 => "false"
			);

			if($this->job_model->add($data))

				redirect('job/index');

		}


	}

}