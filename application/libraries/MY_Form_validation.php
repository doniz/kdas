<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * MY Form Validation Class
 *
 * @package		MY Form Validation Class
 * @subpackage	Libraries
 * @category	Validation
 * @author		Donatas Navidonskis
 * @link		www.doniz.net
 */
class MY_Form_validation extends CI_Form_validation {
	
	public $CI;
	
	/**
	 * - @Constructor
	 *
	 */
	public function __construct(){
	
		parent::__construct();
	
	}
	
	/**
	 * - @exist
	 *
	 * @access	public
	 * @param	string
	 * @param	string $field | table_name.field_name
	 * @return	bool
	 */
	public function exist($string, $field) {
		
		$fields = explode('.', $field);
		
		$this->CI->db->where($fields[1], $string);
		$query = $this->CI->db->get($fields[0]);
		if( $query->num_rows() == 0 ) {
			$this->set_message('exist', 'The %s field information does not exist in the database.');
			
			return false;
		}
		
		return true;
	}
	
}