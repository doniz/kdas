<?php
/**
 * Password Library
 *
 * Genarate password strings. Using sha512 algorithm. Require PHP5 >= 5.1.2
 *
 * @author		Donatas Navidonskis
 * @copyright	Copyright (c) 2012 doniz.net
 * @license		http://www.doniz.net
 * @link		http://www.doniz.net
 * @since		Version 1.0
 */
class Password {
	// blowfish
	private static $algo 		= '$6$';
	// Rounds params
	private static $rounds 		= 'rounds=10000$';
	// salt string
	private static $salt_string	= '0t@3adl$NzUf91t';
	/**
	 * _generate_salt()
	 * Genarate salt string by default 15 char length
	 * 
	 * @public
	 * @param int $max
	 * @return String	
	*/
	public function _generate_salt($max = 15){
		$chars	= human_to_unix(unix_to_human(time(), true)); 
		$chars	.= 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		
		$i		= 0;
		$salt 	= "";
		while ($i < $max) {
			$salt .= $chars{mt_rand(0, (strlen($chars) - 1))};
			$i++;
		}
		return $salt;
	}
	/**
	 * unique_salt()
	 * Mainly for internal use
	 * 
	 * @public, @static
	 * @param none
	 * @return String	
	*/
	public static function _unique_salt(){
		//echo self::$salt_string . "<br/>";
		return crypt(hash("sha512", self::$salt_string, true), mt_rand());
	}
	/**
	 * generate_hash()
	 * This will be used to generate a hash
	 * 
	 * @public, @static
	 * @param "Your Password":String
	 * @return String
	*/
	public static function generate_hash($input_password, $prefix = ''){
		if(CRYPT_SHA512 == 1){
			$input_password = $prefix.$input_password;
			$crypt = crypt($input_password, self::$algo.self::$rounds.self::_unique_salt());
			return str_replace(self::$algo.self::$rounds, '', $crypt);
		}
	}
	/**
	 * check_password()
	 * This will be used to compare a password against a hash
	 * 
	 * @public, @static
	 * @param "Hash Code":String, "Your Password":String
	 * @return true/false
	*/
	public static function check_password($input_hash, $input_password, $prefix = '') {
		$input_password = $prefix.$input_password;
		$input_hash		= self::$algo.self::$rounds.$input_hash;
		$full_salt		= substr($input_hash, 0, 29);
		$new_hash		= crypt($input_password, $full_salt);
		
		if($input_hash == $new_hash)
			return true;
		return false;
	}
}