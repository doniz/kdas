<div style="float:right;margin-top:-40px;"><a href="#register" role="button" data-toggle="modal" class="btn btn-success btn-small" style="cursor:pointer"><i class="icon-user icon-white"></i> Registracija</a></div>
<div id="login" style="width:600px;margin-left:250px;padding:15px;">
	<form class="form-horizontal" action="<?php echo site_url('main/signin');?>" method="post">
		<h5>Prisijunkite</h5>

		<fieldset>
			<div class="control-group">
				<label class="control-label" for="input01">El. Paštas</label>
				<div class="controls">
					<input type="text" name="email" id="input01" value="<?php echo set_value('email');?>" class="input-xlarge" placeholder="El. Paštas">
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" for="input02">Slaptažodis</label>
				<div class="controls">
					<input type="password" name="password" id="input02" value="<?php echo set_value('password');?>" class="input-xlarge" placeholder="Slaptažodis">
				</div>
			</div>

			<div class="control-group">
				<div class="controls">
					<label class="checkbox" for="input03"><input type="checkbox" name="remember" id="input03">Atsiminti Mane</label>
				</div>
			</div>

			<div class="form-actions">
				<button class="btn btn-primary">Prisijungti</button>
				<a href="<?php echo site_url('main/password/email');?>" class="btn btn-inverse">Pamiršote slaptažodį ?</a>
			</div>

		</fieldset>

	</form>

<?php if(!empty($error)) { ?>
<div class="alert alert-error">
	<a class="close" data-dismiss="alert">x</a>
	<h5 class="alert-heading">Klaida!</h5>
	<?php echo $error;?>
</div>
<?php } ?>
</div>

<!-- Modal -->
<form class="form-horizontal" action="<?php echo site_url('accounts');?>" method="post" id="activation-form">
<div id="register" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Saugumo patikrinimas</h3>
  </div>
  <div class="modal-body">

  		<div class="well">
  			<p>Norint registruotis sistemoje turite ivesti 10 simbolių kodą, kurį Jums pateikė administratorius (-ė)
  				arba dėstytojas (-a). </p>
  		</div>

		<fieldset>
			
			<div class="control-group">
				<label class="control-label" for="modal_input">Aktyvacijos kodas</label>
				<div class="controls">
					<input type="text" name="key" id="modal_input" class="input-xlarge" placeholder="Aktyvacijos kodas" maxlength="10">
				</div>
			</div>

		</fieldset>

  </div>
  <div class="modal-footer">
  	<div class="modal-error" style="float:left;display:none;">
	  	<div class="alert alert-error">
	  		<a class="close">x</a>
	  		<span class="text-error" style="text-align:left"></span>
	  	</div>
  	</div>
    <a class="btn btn-small btn-inverse" data-dismiss="modal" aria-hidden="true" style="cursor:pointer" id="form-register-cancel">Atšaukti</a>
    <button class="btn btn-primary btn-small" style="cursor:pointer" id="form-register-submit">Gerai</button>
  </div>
</div>
</form>
<script type="text/javascript">
$(document).ready(function(){

	$('.modal-error a.close').on('click', function(){
		$('.modal-error').hide();
	});

	$('#activation-form').on('submit', function(){

		$('.modal-error').hide();

		$.ajax({
			type: "POST",
			url: <?php echo '"' . site_url('accounts/validate') . '"';?>,
			data: { key: $('#modal_input').val() }
		}).done(function(msg){

			if(msg == 1){
				$('.modal.in').modal('hide');

				    $('.modal').on('hidden', function () {
   					
   						window.location.href = <?php echo '"' . site_url('accounts/signup') . '"';?>;
    				
    				});

			} else if(msg == 2){
				
				$('.text-error').text("Klaida, baigėsi registracijos laikas !");
				$('.modal-error').show();

			} else if(msg == 3){

				$('.text-error').text("Klaida, blogas aktyvacijos kodas arba nėra sukurto registracijos leidimo !");
				$('.modal-error').show();

			} else if(msg == 4){

				$('.text-error').text("Klaida, neįvestas aktyvacijos laukelis !");
				$('.modal-error').show();

			}

		});
	
		return false;
	
	});
});
</script>