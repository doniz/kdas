<div class="row-fluid">
	<div class="span12">
		<h5><?php echo $profile->firstname . " " . $profile->lastname;?><div style="float:right"><a href="<?php echo site_url('main/profile/' . $profile->uid);?>" class="btn" style="cursor:pointer"><i class="icon-edit"></i> Redaguoti</a></div></h5>
    	<hr>
    	<div class="row-fluid">
    		<div class="span6 well"><span class="label label-info">Informacija</span><hr>

<form name="profile" action="<?php echo site_url('main/profile/' . $profile->uid);?>" method="post" class="form-horizontal">

		    	<div class="control-group">
			        <label class="control-label" for="input01">Vardas</label>
			        <div class="controls">
			            <input type="text" name="firstname" id="input01" value="<?php if(set_value('firstname') != '') echo set_value('firstname'); else echo $profile->firstname;?>">
			        </div>
		    	</div>

		    	<div class="control-group">
			        <label class="control-label" for="input02">Pavardė</label>
			        <div class="controls">
			            <input type="text" name="lastname" id="input02" value="<?php if(set_value('lastname') != '') echo set_value('lastname'); else echo $profile->lastname;?>">
			        </div>
		    	</div>

		    	<div class="control-group">
			        <label class="control-label" for="input04">Telefonas</label>
			        <div class="controls">
			            <input type="text" name="phone" id="input04" value="<?php if(set_value('phone') != '') echo set_value('phone'); else echo $profile->phone;?>">
			        	<p class="help-block"><small>861234567</small></p>
			        </div>
		    	</div>

		    	<div class="control-group">
			        <label class="control-label" for="input05">Adresas</label>
			        <div class="controls">
			            <input type="text" name="address" id="input05" value="<?php if(set_value('address') != '') echo set_value('address'); else echo $profile->address;?>">
			        	<p class="help-block"><small>Šiauliai, P. Višinskio g. 19</small></p>
			        </div>
		    	</div>

		    	<div class="form-actions">
					<button type="submit" class="btn btn-primary">Atnaujinti duomenis</button>
				</div>

<?php if(!empty($error)): ?>
<div class="alert alert-error">
	<a class="close" data-dismiss="alert">x</a>
	<h5 class="alert-heading">Klaida!</h5>
	<?php echo $error;?>
</div>
<?php endif;?>
<?php if(!empty($content_success)):?>
<div class="alert alert-success">
	<a class="close" data-dismiss="alert">x</a>
	<h5 class="alert-heading">Sveikiname!</h5>
	<?php echo $content_success;?>
</div>
<?php endif;?>

    		</div>
</form>
    		<div class="span6 well"><span class="label label-info">Vaidmuo</span><hr>
    			<dl class="dl-horizontal">
	    			<dt>Statusas</dt>
	    			<dd><?php if($profile->status == "active") echo "Aktyvuotas"; elseif($profile->status == "inactive") echo "Neaktyvuotas"; else echo "Ištrintas";?></dd>
	    			<dt>Rangas</dt>
	    			<dd><?php echo $rank->fullname;?></dd>
	    			<dt>Teisės</dt>
	    			<dd><?php echo $rank->description;?></dd>
	    		</dl>
    		</div>

<form name="profile" action="<?php echo site_url('main/profile/' . $profile->uid . '/password');?>" method="post" class="form-horizontal">

    		<div class="span6 well" style="float:right"><span class="label label-info">Slaptažodis</span><hr>
    			<div class="control-group">
			        <label class="control-label" for="input06">Dabartinis slaptažodis</label>
			        <div class="controls">
			            <input type="password" name="password" id="input06">
			        </div>
		    	</div>

		    	<div class="control-group">
			        <label class="control-label" for="input07">Naujas slaptažodis</label>
			        <div class="controls">
			            <input type="password" name="new_password" id="input07">
			        </div>
		    	</div>

		    	<div class="control-group">
			        <label class="control-label" for="input08">Pakartokite slaptažodį</label>
			        <div class="controls">
			            <input type="password" name="new_password_conf" id="input08">
			        </div>
		    	</div>

				<div class="form-actions">
					<button type="submit" class="btn btn-primary">Pakeisti slaptažodį</button>
				</div>    

<?php if(!empty($password_error)):?>
<div class="alert alert-error">
	<a class="close" data-dismiss="alert">x</a>
	<h5 class="alert-heading">Klaida!</h5>
	<?php echo $password_error;?>
</div>
<?php endif;?>
<?php if(!empty($password_success)):?>
<div class="alert alert-success">
	<a class="close" data-dismiss="alert">x</a>
	<h5 class="alert-heading">Sveikiname!</h5>
	<?php echo $password_success;?>
</div>
<?php endif;?>

    		</div>
</form>

    	</div>

	</div>
</div>