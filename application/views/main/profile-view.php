<div class="row-fluid">
	<div class="span12">
		<h5><?php echo $profile->firstname . " " . $profile->lastname;?><div style="float:right"><a href="<?php echo site_url('main/profile/' . $profile->uid);?>" class="btn" style="cursor:pointer"><i class="icon-edit"></i> Redaguoti</a></div></h5>
    	<hr>

    	<div class="row-fluid">
    		<div class="span6 well"><span class="label label-info">Informacija</span><hr>
    			<dl class="dl-horizontal">
	    			<dt>Vartotojo vardas</dt>
	    			<dd><?php echo $profile->username;?></dd>
	    			<dt>Vardas</dt>
	    			<dd><?php echo $profile->firstname;?></dd>
	    			<dt>Pavardė</dt>
	    			<dd><?php echo $profile->lastname;?></dd>
	    			<dt>El. Paštas</dt>
	    			<dd><?php echo $profile->email;?></dd>
	    			<dt>Registruotas</dt>
	    			<dd><?php echo date("Y-m-d h:i", $profile->created);?></dd>
	    			<dt>Telefonas</dt>
	    			<dd><?php echo $profile->phone;?></dd>
	    			<dt>Adresas</dt>
	    			<dd><?php echo $profile->address;?></dd>
	    		</dl>
    		</div>

    		<div class="span6 well"><span class="label label-info">Vaidmuo</span><hr>
    			<dl class="dl-horizontal">
	    			<dt>Statusas</dt>
	    			<dd><?php if($profile->status == "active") echo "Aktyvuotas"; elseif($profile->status == "inactive") echo "Neaktyvuotas"; else echo "Ištrintas";?></dd>
	    			<dt>Rangas</dt>
	    			<dd><?php echo $rank->fullname;?></dd>
	    			<dt>Teisės</dt>
	    			<dd><?php echo $rank->description;?></dd>
	    		</dl>
    		</div>
    	</div>
	</div>
</div>