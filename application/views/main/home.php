<style type="text/css">
table tr:first-child th {
	text-align: center;
}
table tr:first-child th:first-child {
	text-align: left;
}
</style>
<?php if(isset($subject) && !empty($subject)):?>
<h4>Jūsų pasirinkta kursinio tema</h4>
<hr>
<div class="well">
<?php if($subject[0]->busy == "false"):?><div style="float:right;padding-bottom:20px;"><a href="<?php echo site_url('main/deselect/' . $subject[0]->sid);?>" style="cursor:pointer" class="btn btn-inverse" title="Atšaukti" onClick="return confirm('Ar tikrai norite ištrinti įraša?')"><i class="icon-remove icon-white"></i> </a></div><?php endif;?>
<table class="table table-striped table-bordered">

	<tr>
		<th colspan="2">Dėstytojas</th>
		<th colspan="2">Temos Pavadinimas</th>
	</tr>

	<tr>
		<td colspan="2" style="width:200px">Vardas / Pavardė</td>
		<td colspan="2" style="text-align:center"><?php echo $subject[0]->title;?></td>
	</tr>

	<tr><?php $lecture = $this->account->get(array("uid" => $subject[0]->lecture_id));?>
		<td style="width:100px"><?php echo $lecture->firstname;?></td>
		<td style="width:100px"><?php echo $lecture->lastname;?></td>
		<th colspan="2">Temos Aprašymas</th>
	</tr>
	<tr>
		<td></td>
		<td style="border-left:none"></td>
		<td colspan="2" style="border-left:none"><?php echo $subject[0]->description;?></td>
	</tr>
	<tr>
		<th colspan="2" style="text-align:center"><?php echo date("Y.m.d", $subject[0]->date);?></th>
		<th colspan="2" style="text-align:center"><?php if($subject[0]->busy == "true") echo "Patvirtinta"; else echo "Nepatvirtinta";?></th>
	</tr>

</table>
</div>
<?php endif;?>

<?php if(isset($subjects)):?>
<h4>Kursinių darbų temos</h4>
<hr>
<?php foreach($subjects as $s):?>
<table class="table table-striped table-bordered">
	<tr>
		<th colspan="2">Dėstytojas</th>
		<th colspan="2">Temos Pavadinimas</th>
	</tr>

	<tr>
		<td colspan="2" style="width:200px">Vardas / Pavardė</td>
		<td colspan="1" style="text-align:center"><?php echo $s->title;?></td>
		<td style="text-align:right;width:100px;"><?php if(empty($subject)):?><a href="<?php echo site_url('main/select/' . $s->sid);?>" class="btn btn-primary"><i class="icon-plus icon-white"></i> Pasirinkti Temą</a><?php endif;?></td>
	</tr>

	<tr><?php $lecture = $this->account->get(array("uid" => $s->lecture_id));?>
		<td style="width:100px"><?php echo $lecture->firstname;?></td>
		<td style="width:100px"><?php echo $lecture->lastname;?></td>
		<th colspan="2">Temos Aprašymas</th>
	</tr>
	<tr>
		<td></td>
		<td style="border-left:none"></td>
		<td colspan="2" style="border-left:none"><?php echo $s->description;?></td>
	</tr>
	<tr>
		<th colspan="2" style="text-align:center"><?php echo date("Y.m.d", $s->date);?></th>
		<th colspan="2" style="text-align:center"></th>
	</tr>
</table>
<?php endforeach;?>
<?php endif;?>