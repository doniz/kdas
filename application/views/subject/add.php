<form class="form-horizontal" action="<?php echo site_url('subject/add');?>" method="post">
    <legend>Kurti tematiką<span style="float:right"><small>Liko temų <?php echo count($subjects);?> / <?php if(count($amount) > 0) echo $amount->amount; else echo 0;?></small></span></legend>
  <fieldset>
  	<?php if(count($amount) == 0):?>
  	<p>Norint kurti temas jums reikia teirautis pas vedėja, kuri (-is) nurodys paskirtų darbų kiekį.
  		<br/>Vedėjų el. p. adresai:</p>
  		<table class="table">
  			<tr>
  				<th>Vardas Pavardė</th>
  				<th>El. Pašto adresas</th>
  				<th>Telefonas</th>
  			</tr>
  			<?php foreach($manager as $m):?>
  			<tr>
  				<td><?php echo $m->firstname . " " . $m->lastname;?></td>
  				<td><?php echo $m->email;?></td>
  				<td><?php if(empty($m->phone)) echo "<i class=\"icon-minus\"></i>"; else echo $m->phone;?></td>
  			</tr>
  			<?php endforeach;?>
  		</table>
  	<?php elseif($amount->amount != 0 && $amount->amount == count($subjects)):?>
  	<p>Daugiau negalite kurti tematikų. Jums buvo skirtos <?php echo $amount->amount;?> tematikos (-ą).</p>
  	<?php else:?>
    <div class="control-group">
        <label class="control-label" for="input01">Pavadinimas *</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="input01" name="title" value="<?php echo set_value('title');?>">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="input02">Apibūdinimas *</label>
        <div class="controls">
        	<textarea name="description" id="input02" class="input-xlarge" rows="6"><?php echo set_value('description');?></textarea>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="select02">Studentas</label>
        <div class="controls">
        <?php

        $options = array();
        foreach($students as $student){
            if(!array_key_exists(0, $options))
                $options[0] = "Pažymėkite jei nereikia";

          $options[$student->uid] = $student->firstname . " " . $student->lastname;
        }

        echo form_dropdown("student_id", $options, '', 'id="select02"');

        ?>
        <p class="help-block">Palikite tuščia, jei nėra suderinta su studentu</p>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label">* - Privalomi laukai</label>
    </div>

    <div class="form-actions">
        <button type="submit" class="btn btn-primary">Gerai</button>
    </div>
	<?php endif;?>
  </fieldset>
</form>
<script type="text/javascript">
$(document).ready(function(){

	$('#input03').click(function(){

		if($(this).attr('checked') == "checked"){
			$('#select02').removeClass('disabled');
			$('div.btn-group a').removeClass('disabled');
		} else {
			$('#select02').addClass('disabled');
			$('div.btn-group a').addClass('disabled');
		}
	});
});
</script>
<?php
if(!empty($error)) { ?>
<div class="alert alert-error">
    <a class="close" data-dismiss="alert">x</a>
    <h5 class="alert-heading">Klaida!</h5>
    <?php echo $error;?>
</div>
<?php } ?>
</div>