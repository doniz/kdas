<?php $show_message = "<div class='well' style='text-align:center'>Duomenų nėra</div>";?>
<?php if(isset($subjects_app)):?>
<?php $need_show_message = true;?>
<h4>Patvirtintos temos</h4>
<hr>
<?php foreach($subjects_app as $approve):?>
<?php $need_show_message = false;?>
<table class="table table-striped table-bordered">
	<tr>
		<th colspan="2">Pagrindinė informacija</th>
		<td colspan="3" style="border-left:none;text-align:right;padding-right:30px;"><?php $lecture = $this->account->get(array('uid' => $approve->lecture_id)); echo $lecture->firstname . " " . $lecture->lastname;?></td>
	</tr>
	<tr>
		<td>Pavadinimas</td>
		<td>Apibūdinimas</td>
		<td>Studentas</td>
		<td>Patvirtinta?</td>
		<td>Diena</td>
	</tr>
	<tr>
		<td><?php echo $approve->title;?></td>
		<td><?php echo $approve->description;?></td>
		<td><?php $student = $this->account->get(array('uid' => $approve->student_id)); echo $student->firstname . " " . $student->lastname;?></td>
		<td style="text-align:center"><?php if($approve->busy == 'true') echo 'Taip'; else echo 'Ne';?></td>
		<td><?php echo date("Y.m.d", $approve->date);?></td>
	</tr>
	<tr>
		<td style="text-align:right" colspan="5"><a href="<?php echo site_url('subject/approve/edit/' . $approve->sid);?>" class="btn btn-success btn-small" style="cursor:pointer"><i class="icon-edit icon-white"></i> Redaguoti</a> <a href="<?php echo site_url('subject/approve/cancel/' . $approve->sid);?>" class="btn btn-danger btn-small" style="cursor:pointer" onClick="return confirm('Ar tikrai norite atšaukti patvirtinimą ?')">Atšaukti</a></td>
	</tr>
</table>
<?php endforeach;?>
<?php if($need_show_message) echo $show_message;?>
<?php endif;?>


<?php if(isset($subjects)):?>
<?php $need_show_message = true;?>
<h4>Laukia patvirtintimo</h4>
<hr>
<?php foreach($subjects as $subject):?>
<?php if($subject->student_id != 0):?>
<?php $need_show_message = false;?>
<table class="table table-striped table-bordered">
	<tr>
		<th colspan="2">Pagrindinė informacija</th>
		<td colspan="3" style="border-left:none;text-align:right;padding-right:30px;"><?php $lecture = $this->account->get(array('uid' => $subject->lecture_id)); echo $lecture->firstname . " " . $lecture->lastname;?></td>
	</tr>
	<tr>
		<td>Pavadinimas</td>
		<td>Apibūdinimas</td>
		<td>Studentas</td>
		<td>Patvirtinta?</td>
		<td>Diena</td>
	</tr>
	<tr>
		<td><?php echo $subject->title;?></td>
		<td><?php echo $subject->description;?></td>
		<td><?php $student = $this->account->get(array('uid' => $subject->student_id)); echo $student->firstname . " " . $student->lastname;?></td>
		<td style="text-align:center"><?php if($subject->busy == 'true') echo 'Taip'; else echo 'Ne';?></td>
		<td><?php echo date("Y.m.d", $subject->date);?></td>
	</tr>
	<tr>
		<td style="text-align:right" colspan="5"><a href="<?php echo site_url('subject/approve/edit/' . $subject->sid);?>" class="btn btn-success btn-small" style="cursor:pointer"><i class="icon-edit icon-white"></i> Redaguoti</a> <a href="<?php echo site_url('subject/approve/apply/' . $subject->sid);?>" class="btn btn-primary btn-small" style="cursor:pointer">Patvirtinti</a></td>
	</tr>
</table>
<?php endif;?>
<?php endforeach;?>
<?php if($need_show_message) echo $show_message;?>
<?php endif;?>

<?php if(isset($subjects)):?>
<?php $need_show_message = true;?>
<h4>Likusios temos</h4>
<hr>
<?php foreach($subjects as $subject):?>
<?php if($subject->student_id == 0):?>
<?php $need_show_message = false;?>
<table class="table table-striped table-bordered">
	<tr>
		<th colspan="2">Pagrindinė informacija</th>
		<td colspan="3" style="border-left:none;text-align:right;padding-right:30px;"><?php $lecture = $this->account->get(array('uid' => $subject->lecture_id)); echo $lecture->firstname . " " . $lecture->lastname;?></td>
	</tr>
	<tr>
		<td>Pavadinimas</td>
		<td>Apibūdinimas</td>
		<td>Studentas</td>
		<td>Patvirtinta?</td>
		<td>Diena</td>
	</tr>
	<tr>
		<td><?php echo $subject->title;?></td>
		<td><?php echo $subject->description;?></td>
		<td style="text-align:center"><i class="icon-minus"></i></td>
		<td style="text-align:center"><?php if($subject->busy == 'true') echo 'Taip'; else echo 'Ne';?></td>
		<td><?php echo date("Y.m.d", $subject->date);?></td>
	</tr>
</table>
<?php endif;?>
<?php endforeach;?>
<?php if($need_show_message) echo $show_message;?>
<?php endif;?>