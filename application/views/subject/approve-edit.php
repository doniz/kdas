<form name="approve-edit" action="<?php echo site_url('subject/approve/edit/' . $subject->sid);?>" method="post" class="form-horizontal">
<table class="table table-striped table-bordered">
	<tr>
		<th colspan="2">Pagrindinė informacija</th>
		<td colspan="3" style="border-left:none;text-align:right;padding-right:30px;"><?php $lecture = $this->account->get(array('uid' => $subject->lecture_id)); echo $lecture->firstname . " " . $lecture->lastname;?></td>
	</tr>
	<tr>
		<td>Pavadinimas</td>
		<td>Apibūdinimas</td>
		<td>Studentas</td>
		<td>Patvirtinta?</td>
		<td>Diena</td>
	</tr>
	<tr>
		<td><textarea name="title" style="height:180px;width:200px;"><?php echo $subject->title;?></textarea></td>
		<td><textarea name="description" style="height:180px;width:350px;"><?php echo $subject->description;?></textarea></td>
		<td><?php $student = $this->account->get(array('uid' => $subject->student_id)); echo $student->firstname . " " . $student->lastname;?></td>
		<td style="text-align:center"><?php if($subject->busy == 'true') echo 'Taip'; else echo 'Ne';?></td>
		<td><?php echo date("Y.m.d", $subject->date);?></td>
	</tr>
	<tr>
		<td style="text-align:right" colspan="5"><a href="<?php echo site_url('subject/approve');?>" class="btn btn-inverse btn-small" style="cursor:pointer">Atšaukti</a> <input type="submit" class="btn btn-primary btn-small" name="approve_edit" value="Redaguoti"></td>
	</tr>
</table>