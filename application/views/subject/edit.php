<form class="form-horizontal" action="<?php echo site_url('subject/edit/' . $subject->sid);?>" method="post">
    <legend>Redaguoti tematiką</legend>
  <fieldset>
    <div class="control-group">
        <label class="control-label" for="input01">Pavadinimas *</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="input01" name="title" value="<?php if(!set_value('title')) echo $subject->title; else echo set_value('title');?>">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="input02">Apibūdinimas *</label>
        <div class="controls">
        	<textarea name="description" id="input02" class="input-xlarge" rows="6"><?php if(!set_value('description')) echo $subject->description; else echo set_value('description');?></textarea>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="select02">Studentas</label>
        <div class="controls">
        <?php

        $options = array();
        foreach($students as $student){
            if(!array_key_exists(0, $options))
                $options[0] = "Pažymėkite jei nereikia";

          $options[$student->uid] = $student->firstname . " " . $student->lastname;
        }

        echo form_dropdown("student_id", $options, '', 'id="select02"');

        ?>
        <p class="help-block">Palikite tuščia, jei nėra suderinta su studentu</p>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label">* - Privalomi laukai</label>
    </div>

    <div class="form-actions">
        <button type="submit" class="btn btn-primary">Gerai</button>
    </div>
  </fieldset>
</form>
<?php
if(!empty($error)) { ?>
<div class="alert alert-error">
    <a class="close" data-dismiss="alert">x</a>
    <h5 class="alert-heading">Klaida!</h5>
    <?php echo $error;?>
</div>
<?php } ?>
</div>