<h4>Jūsų sukūrtos tematikos</h4>
<hr>
<table class="table table-striped table-bordered">
	<tr>
		<th></th>
		<th colspan="5">Pagrindinė informacija</th>
		<th colspan="2">Veiksmai</th>
	</tr>
	<tr>
		<td>#</td>
		<td>Pavadinimas</td>
		<td>Apibūdinimas</td>
		<td>Studentas</td>
		<td>Patvirtinta?</td>
		<td>Diena</td>
		<td colspan="2"></td>
	</tr>
<?php foreach($subjects as $s):?>

<?php if($s->busy == "true"):?>

	<tr style="color:#C0C0C0">

<?php else:?>

	<tr>

<?php endif;?>

		<td><?php echo $s->sid;?></td>
<?php
	
	$student_name = "<i class='icon-minus'></i>";

	if(!empty($s->student_id)){

		$student = $this->account->get(array('uid' => $s->student_id));

		$student_name = $student->firstname . " " . $student->lastname;

	}

?>
		<td><?php echo $s->title;?></td>
		<td><?php echo $s->description;?></td>
		<td style="text-align:center"><?php echo $student_name;?></td>
		<td style="text-align:center"><?php echo ($s->busy == 'true') ? "<i class='icon-plus'></i>" : "<i class='icon-minus'></i>";?></td>
		<td style="text-align:center"><?php echo date('Y.m.d',$s->date);?></td>

		<?php if($s->busy == "true"):?>
		
		<td colspan="2" style="text-align:center">Tema patvirtinta</td>
		
		<?php else:?>
		
		<td style="text-align:center"><?php echo anchor(site_url('subject/edit/'.$s->sid), '<i class="icon-edit icon-white"></i> Redaguoti', array('class' => "btn btn-primary btn-small"));?></td>
		<td style="text-align:center"><?php echo anchor(site_url('subject/delete/'.$s->sid), '<i class="icon-trash icon-white"></i> Trinti', array('onClick' => "return confirm('Ar tikrai norite ištrinti įraša?')", 'class' => "btn btn-danger btn-small"));?></td>
		
		<?php endif;?>
	</tr>
<?php endforeach;?>
</table>