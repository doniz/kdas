<form class="form-horizontal" action="<?php echo site_url('auth/permission/add');?>" method="post">
	<legend>Kurti naują puslapio autorizaciją ir navigacijos vardus</legend>
	<fieldset>

		<h4 class="well">Pagrindinė informacija</h4>

		<div class="control-group" style="float:right;width:400px;">
    		<span class="label label-important" style="float:left"><i class="icon-plus icon-white"></i></span> <p class="well"> Reikalingi laukeliai</p>
    	</div>

		<div class="control-group">
	        <label class="control-label" for="select01"><span class="label label-important" style="float:left"><i class="icon-plus icon-white"></i></span> Pasirinkite Teises</label>
	        <div class="controls">
	            <select name="rid[]" multiple="multiple" id="select01" style="height:100px">
	            <?php foreach($rid as $rid):?>
					<option value="<?php echo $rid->rid;?>" <?php echo set_select('rid[]', $rid->rid);?>><?php echo $rid->fullname;?></option>
				<?php endforeach;?>
	            </select>
	           	 <p class="help-block"><small>Nuspauskite <span class="label label-info">CTRL</span> arba <span class="label label-info">CMD</span> ir pažymėkite.</small></p>
	        </div>
    	</div>

    	<div class="control-group">
	        <label class="control-label" for="input01"><span class="label label-important" style="float:left"><i class="icon-plus icon-white"></i></span> URL</label>
	        <div class="controls">
	            <input type="text" name="url" id="input01" value="<?php echo set_value('url');?>" class="input-xlarge">
	        	<p class="help-block"><small><i>auth/permission</i></small></p>
	        </div>
    	</div>

    	<h4 class="well">Navigacijos informacija</h4>

    	<div class="control-group">
	        <label class="control-label" for="input03"><span class="label label-important" style="float:left"><i class="icon-plus icon-white"></i></span> Pavadinimas</label>
	        <div class="controls">
	            <input type="text" name="link_name" id="input03" value="<?php echo set_value('link_name');?>"  class="input-xlarge">
	        	<p class="help-block"><small><i>Pagrindinis, Mano Profilis</i></small></p>
	        </div>
    	</div>

		<div class="control-group">
			<label class="control-label" for="select01">Pakategorės</label>
			<div class="controls">
					<?php $permis = $this->permission->get();?>
					<?php

						$new_array = array();

						foreach($permis as $p){
							if(preg_match('/\w+\/\w+/', $p->url) || preg_match('/\w+/', $p->url))
								$new_array[] = $p->url;

						}

						$new_array = array_unique($new_array);
					?>
				<select name="sub_url" id="select01">
					<option value="">Pasirinkite...</option>
					<?php foreach($new_array as $arr):?>
					<option value="<?php echo $arr;?>"><?php echo $arr;?></option>
					<?php endforeach;?>
				</select>
				<p class="help-block"><small><i>Susieti su puslapiu ir padaryti "sub" puslapį</i></small></p>
			</div>
		</div>

    	<div class="control-group">
	        <label class="control-label" for="input04">Įkonėlė <br/> <small>Paveikslėlis prie nuorodos</small></label>
	        <div class="controls">
	            <input type="text" name="icon_name" id="input04" value="<?php echo set_value('icon_name');?>"  class="input-xlarge">
	        	<p class="help-block"><i class="icon-home"></i> <small><i>icon-home</i></small>, <i class="icon-edit"></i> <small><i>icon-edit</i> <a href="<?php echo site_url('main/icons');?>" target="_blank">Įkonėlės</a></small></p>
	        </div>
    	</div>

    	<div class="control-group">
	        <label class="control-label" for="input10"><span class="label label-important" style="float:left"><i class="icon-plus icon-white"></i></span> Kategorija</label>
	        <div class="controls">
	        	<label class="radio">
	        		<input type="radio" name="category" id="input10" value="puslapiai" checked>Puslapiai
	        	</label>
	        	<label class="radio">
	        		<input type="radio" name="category" id="input10" value="vartotojai">Vartotojai
	        	</label>
	        </div>
    	</div>

		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Išsaugoti</button>
		</div>

	</fieldset>
</form>
<?php
if(!empty($error)) { ?>
<div class="alert alert-error">
	<a class="close" data-dismiss="alert">x</a>
	<h5 class="alert-heading">Klaida!</h5>
	<?php echo $error;?>
</div>
<?php } ?>
