<table class="table table-striped table-bordered table-condensed">
	<tr>
		<th></th>
		<th colspan="4">Pagrindinė informacija</th>
		<th colspan="2">Veiksmai</th>
	</tr>
	<tr>
		<td>#</td>
		<td>Priėjimas</td>
		<td>URL</td>
		<td>Pakategorė</td>
		<td>Pavadinimas</td>
		<td colspan="2"></td>
	</tr>
<?php foreach($permissions as $p):?>
	<tr>
		<td><?php echo $p->pid;?></td>
<?php
	
	$p->rid = explode(',', $p->rid);

	for($i = 0; $i < count($p->rid); $i++){
		
		$rank = $this->permission->get_rank(array('rid' => $p->rid[$i]));
		$p->rid[$i] = $rank->name;

	}

	$p->rid = implode(',', $p->rid);
?>
		<td><?php echo str_replace(',', ',<br/>', $p->rid);?></td>
		<td><?php echo (!$p->url) ? "<i>Null</i>" : "<i>".$p->url."</i>";?></td>
		<td><?php echo (!$p->sub_url) ? "<i>Null</i>" : "<i class=\"label label-important\">".$p->sub_url."</i>";?></td>
		<td><?php echo (!$p->link_name) ? "<i>Null</i>" : "<i>".$p->link_name."</i>";?></td>
		<td style="text-align:center"><?php echo anchor(site_url('auth/permission/edit/'.$p->pid), '<i class="icon-edit icon-white"></i> Redaguoti', array('class' => "btn btn-primary btn-small"));?></td>
		<td style="text-align:center"><?php echo anchor(site_url('auth/permission/delete/'.$p->pid), '<i class="icon-trash icon-white"></i> Trinti', array('onClick' => "return confirm('Ar tikrai norite ištrinti įraša?')", 'class' => "btn btn-danger btn-small"));?></td>
	</tr>
<?php endforeach;?>
</table>