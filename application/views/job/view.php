<h4>Dėstytojams priskirtos temos</h4>
<hr>
<table class="table table-striped table-bordered">
	<tr>
		<th></th>
		<th colspan="2" style="text-align:center">Darbų kiekis</th>
		<th></th>
		<th colspan="2" style="text-align:center">Veiksmai</th>
	</tr>
	<tr>
		<td>Dėstytojas</td>
		<td style="text-align:center">Užpildyta temų</td>
		<td style="text-align:center">Skirta temų</td>
		<td style="text-align:center">Užimtos temos?</td>
		<td colspan="2"></td>
	</tr>
<?php
	
	if(isset($subjects) && isset($amount) && isset($lectures)){

		foreach($lectures as $x){

			$data = array();

			$data["name"] = $x->firstname . " " . $x->lastname;

			foreach($subjects as $y){

				if($x->uid == $y->lecture_id){

					if( !isset($data["sub1"]) )
						$data["sub1"] = 0;

					$data["sub1"]++;

				}

				if($x->uid == $y->lecture_id && $y->busy == "true"){

					if( !isset($data["busy"]) )
						$data["busy"] = 0;

					$data["busy"]++;

				}

			}

			foreach($amount as $z){

				if($x->uid == $z->lecture_id && $z->closed == "false"){

					$data["sub2"] = $z->amount;
					$data["id"]	  = $z->sa_id;

				}

			}

		if(isset($data["sub2"]) && isset($data["id"])){

		$suma = 0;

		if( !isset($data["busy"]) )
			$data["busy"] = 0;

		if( !isset($data["sub1"]) )
			$data["sub1"] = 0;

		$suma = (number_format($data["busy"], 1)/100) / (number_format($data["sub2"], 1) / 100);
		$suma = number_format(100 * $suma, 1) . " %";

		?>
	<tr>
		<td><?php echo $data["name"];?></td>
		<td style="text-align:center"><?php echo $data["sub1"];?></td>
		<td style="text-align:center"><?php echo $data["sub2"];?></td>
		<td style="text-align:center"><?php echo $data["busy"] . " - " . $suma;?></td>
		<td style="text-align:center"><?php echo anchor(site_url('job/edit/' .$data['id']), '<i class="icon-edit icon-white"></i> Redaguoti', array('class' => "btn btn-primary btn-small"));?></td>
		<td style="text-align:center"><?php if(!$this->job_model->secure('job/delete')) echo "<a href=\"" . current_url() . "\" class=\"btn btn-danger btn-small disabled\"><i class=\"icon-trash icon-white\"></i> Trinti</a>"; else echo anchor(site_url('job/delete/' .$data['id']), '<i class="icon-trash icon-white"></i> Trinti', array('onClick' => "return confirm('Ar tikrai norite ištrinti įraša? Visos temos taip pat bus pašalintos !')", 'class' => "btn btn-danger btn-small"));?></td>
	</tr>
		<?php
		}

		}

	}
?>
</table>

<h4>Dėstytojams nepriskirtos temos</h4>
<hr>
<table class="table table-striped table-bordered">
	<tr>
		<th colspan="2">Dėstytojas<br/><small>Vardas / Pavardė</small></th>
		<th>Veiksmai</th>
	</tr>
<?php
	
	if(isset($amount) && isset($lectures)){

		foreach($lectures as $l){

			$data = array();
			$match = false;

			foreach($amount as $a){

				if($l->uid == $a->lecture_id && $a->closed == "false")
					$match = true;

			}

			if(!$match){

				$data["firstname"] = $l->firstname;
				$data["lastname"]  = $l->lastname;
				$data["uid"]	   = $l->uid;
			}

	
	if( isset($data["firstname"]) && isset($data["lastname"]) && isset($data["uid"]) ){
?>
	<tr>
		<td><?php echo $data["firstname"];?></td>
		<td><?php echo $data["lastname"];?></td>
		<td style="text-align:center"><?php if(!$this->job_model->secure('job/add')) echo '<a href="'.current_url().'" class="btn btn-success btn-small disabled"><i class="icon-plus icon-white"></i> Pridėti</a>'; else echo anchor(site_url('job/add/' . $data["uid"]), '<i class="icon-plus icon-white"></i> Pridėti', array('class' => "btn btn-success btn-small"));?></td>
	</tr>
<?php
	}
	}
	}
?>
</table>