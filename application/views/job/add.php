<?php if(isset($lecture)):?>
<form class="form-horizontal" action="<?php echo site_url('job/add/' . $lecture->uid);?>" method="post">
<?php else:?>
<form class="form-horizontal" action="<?php echo site_url('job/add');?>" method="post">
<?php endif;?>
  <legend>Skirti darbo temas<div style="float:right"><small><?php if(isset($lecture)) echo $lecture->firstname . " " . $lecture->lastname;?></small></div></legend>
  <fieldset>

    <div class="control-group">
      <label class="control-label" for="input01">Temų kiekis *</label>
      <div class="controls">
        <input type="text" class="input" id="input01" name="amount" value="">
      </div>
    </div>

    <?php if( isset($lecture) ):?>
    <input type="hidden" name="lecture_id" class="input" value="<?php echo $lecture->uid;?>">
    <?php endif;?>

    <?php if( isset($lectures) ):?>
    <div class="control-group">
      <label class="control-label" for="select01">Dėstytojas *</label>
      <div class="controls">
        <select name="lecture_id" id="select01">
          <?php foreach($lectures as $l):?>
          <option value="<?php echo $l->uid;?>"><?php echo $l->firstname . " " . $l->lastname;?></option>
          <?php endforeach;?>
        </select>
      </div>
    </div>
    <?php endif;?>

    <div class="control-group">
      <label class="control-label">* - Privalomi laukai</label>
    </div>

    <div class="form-actions">
        <button type="submit" class="btn btn-primary">Gerai</button>
    </div>

  </fieldset>
</form>

<?php if(!empty($error)):?>
<div class="alert alert-error">
    <a class="close" data-dismiss="alert">x</a>
    <h5 class="alert-heading">Klaida!</h5>
    <?php echo $error;?>
</div>
<?php endif;?>
</div>