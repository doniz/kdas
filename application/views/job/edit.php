<form class="form-horizontal" action="<?php echo site_url('job/edit/' . $amount->sa_id);?>" method="post">
  <legend>Keisti darbo temų kiekį<div style="float:right"><small><?php if(isset($lecture)) echo $lecture->firstname . " " . $lecture->lastname;?></small></div></legend>
  <fieldset>

    <div class="control-group">
      <label class="control-label" for="input01">Temų kiekis *</label>
      <div class="controls">
        <input type="text" class="input" id="input01" name="amount" value="<?php echo $amount->amount;?>">
        <p class="help-block">Jei nurodysite <b><i>mažiau</i></b>, dėstytojo <br/>
          įrašytos temos (<b>nepatvirtintos</b>) bus<br/>
          pašalintos. Jei visos bus <b>patvirtintos</b>,<br/>
          tuomet jums neleis panaikinti šių temų.</p>
      </div>
    </div>

    <?php if( isset($lecture) ):?>
    <input type="hidden" name="lecture_id" class="input" value="<?php echo $lecture->uid;?>">
    <?php endif;?>

    <div class="control-group">
      <label class="control-label">* - Privalomi laukai</label>
    </div>

    <div class="form-actions">
        <button type="submit" class="btn btn-primary">Gerai</button>
    </div>

  </fieldset>
</form>

<?php if(!empty($error)):?>
<div class="alert alert-error">
    <a class="close" data-dismiss="alert">x</a>
    <h5 class="alert-heading">Klaida!</h5>
    <?php echo $error;?>
</div>
<?php endif;?>
</div>