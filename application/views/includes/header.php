<?php
	
	if($this->session->userdata('account'))
	$member = $this->account->get(
		array('email' => $this->session->userdata('account'))
	);

?>
<!DOCTYPE html> 
<html lang="lt">
<head>
	<!-- Meta Information -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Kursinių darbų apskaitos sistema</title>
	<meta name="author" content="Donatas Navidonskis">
	<meta name="robots" content="all">
	<meta name="revisit-after" content="1 days">
	<!-- Cascading Style Sheet (CSS) -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-responsive.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/google-bootstrap.css');?>">
	<link rel="stylesheet" media="screen" type="text/css" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css">
	<style type="text/css">
	.footer {
		background-color: whiteSmoke;
	    position:static;
	    bottom: 0 !important;
	    width: 85%;
	    margin-left:auto;
	    margin-right:auto;
		text-align:center;
		margin-top:50px;
	}
	#ui-datepicker-div {
		font-family: "Trebuchet MS", "Helvetica", "Arial",  "Verdana", "sans-serif";
		font-size: 12px;
		line-height: 15px;
	}
	.ui-datepicker-header {
		height:25px;
	}
	.ui-datepicker-title select {
		height:20px;
	}
	</style>
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
</head>
<body>
<div class="container">

<!-- Header -->
<div class="row-fluid" style="margin-bottom:50px;border-bottom:1px solid #DDD;">
	<span class="span12">
		<span class="span6" style="padding-left:50px;">
			<img src="<?php echo base_url('assets/img/su.png');?>" alt="Šiaulių Universitetas" style="padding:10px">
		</span>
		<?php if(isset($member)):?>
		<span class="span6" style="padding:10px">
			<p>Sveiki, <?php echo $member->firstname;?></p><hr>
			<?php $rank = $this->permission->get_rank(array('rid' => $member->rid));?>
			<p>Jūsų rangas: <span class="label"><?php echo $rank->fullname;?></span></p>
			<p>Apie rangą: <?php echo $rank->description;?></p>
		</span>
		<?php else:?>
		<span class="span6 well" style="padding:10px">
			Kursinių darbų apskaitos sistema
		</span>
		<?php endif;?>
	</span>
</div>

<div class="container-fluid">
  <div class="row-fluid">

    <div class="span2">
      <!-- Sidebar content -->
          <?php if(isset($navigation) && !empty($navigation)):?>
      	<div class="navigation">
      		<ul class="nav nav-list">
      	<?php foreach($navigation as $key => $array):?>
      			<li class="nav-header">
      				<?php echo $key;?>
      			</li>
      			<?php foreach($array as $row):?>
		      		<?php if(isset($row['drop'])):?>
		      			<li class="dropdown">
		      				<a href="<?php echo site_url($row['url']);?>" data-toggle="dropdown"><?php echo htmlspecialchars_decode($row['name']);?><b class="caret"></b></a>
		      				<ul class="dropdown-menu">
		      					<?php foreach($row['drop'] as $dropdown):?>
		      						<li><a href="<?php echo site_url($dropdown['url']);?>"><?php echo htmlspecialchars_decode($dropdown['name']);?></a></li>
		      					<?php endforeach;?>
		      				</ul>
		      			</li>
		      		<?php else:?>
      			<li<?php if(site_url($row['url']) == current_url()) echo " class=\"active\"";?>>
      				<a href="<?php echo site_url($row['url']);?>"><?php echo htmlspecialchars_decode($row['name']);?></a>
      			</li>
      				<?php endif;?>
      		<?php endforeach;?>
      	<?php endforeach;?>
      		</ul>
      	</div>
      	<?php endif;?>
    </div>

    <?php if(!empty($navigation)):?>
    	<div class="span10">
    <?php endif;?>
      <!-- Body content -->