<script src="<?php echo base_url();?>assets/js/google-select-dropdown.js"></script>
<script src="<?php echo base_url();?>assets/js/google-select.js"></script>
<script src="<?php echo base_url();?>assets/js/application.js"></script>

    <?php if(!empty($navigation)):?>
    </div>
	<?php endif;?>

  </div>

</div>
  	<div id="footer" class="footer well">
		<div class="row-fluid">
		  	<div class="span12 muted">
				Visos teisės &copy; <a href="http://www.doniz.net" target="_blank">Donatas Navidonskis</a> &bull; PHP Karkaskas (Framework) &copy; <a href="http://www.codeigniter.com" target="_blank">Codeigniter</a><br/>
				Vartotojo sąsaja (Dizainas) &copy; <a href="http://twitter.github.com/bootstrap/index.html" target="_blank">CSS3 Twitter Bootstrap</a> &bull; Įkonėlės &copy; <a href="http://glyphicons.com" target="_blank">Glyphicons Free</a><br/>
				<a href="http://www.su.lt" target="_blank">Šiaulių Universitetas</a> &bull; <a href="http://ik.su.lt" target="_blank">Informatikos Katedra</a>
		  	</div>
		</div>
	</div>
</div>
</body>
</html>