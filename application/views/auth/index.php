<h4>Duomenų bazėje esantys duomenys</h4>
<hr>
<h5>Vartotojai</h5>
<hr>
<table class="table table-striped table-bordered table-vertical">
	<tr>
		<th style="text-align:left">Rangas</th>
		<th style="text-align:center">Registruotų vartotojų</th>
		<th style="text-align:center">Aktyvuoti</th>
		<th style="text-align:center">Neaktyvuoti</th>
	</tr>
<?php foreach($members as $m):?>
	<tr>
		<td style="text-align:left"><?php $rank = $this->account->get_rank($m['rid']); echo $rank->fullname;?></td>
		<td style="text-align:center"><?php echo $m['count'];?></td>
		<td style="text-align:center"><?php echo $m['active'];?></td>
		<td style="text-align:center"><?php echo $m['inactive'];?></td>
	</tr>
<?php endforeach;?>
</table>

<h5>Temų kiekis</h5>
<hr>
<table class="table table-striped table-bordered">
	<tr>
		<th>Dėstytojas</th>
		<th style="text-align:center">Užpildyta temų</th>
		<th style="text-align:center">Skirta temų</th>
		<th style="text-align:center">Užimtos temos?</th>
	</tr>
<?php
	
	if(isset($subjects) && isset($amount) && isset($lectures)){

		foreach($lectures as $x){

			$data = array();

			$data["name"] = $x->firstname . " " . $x->lastname;

			foreach($subjects as $y){

				if($x->uid == $y->lecture_id){

					if( !isset($data["sub1"]) )
						$data["sub1"] = 0;

					$data["sub1"]++;

				}

				if($x->uid == $y->lecture_id && $y->busy == "true"){

					if( !isset($data["busy"]) )
						$data["busy"] = 0;

					$data["busy"]++;

				}

			}

			foreach($amount as $z){

				if($x->uid == $z->lecture_id && $z->closed == "false"){

					$data["sub2"] = $z->amount;
					$data["id"]	  = $z->sa_id;

				}

			}

		if(isset($data["sub2"]) && isset($data["id"])){

		$suma = 0;

		if( !isset($data["busy"]) )
			$data["busy"] = 0;

		if( !isset($data["sub1"]) )
			$data["sub1"] = 0;

		$suma = (number_format($data["busy"], 1)/100) / (number_format($data["sub2"], 1) / 100);
		$suma = number_format(100 * $suma, 1) . " %";

		?>
	<tr>
		<td><?php echo $data["name"];?></td>
		<td style="text-align:center"><?php echo $data["sub1"];?></td>
		<td style="text-align:center"><?php echo $data["sub2"];?></td>
		<td style="text-align:center"><?php echo $data["busy"] . " - " . $suma;?></td>
	</tr>
		<?php
		}

		}

	}
?>
</table>