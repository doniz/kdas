<form class="form-horizontal" action="<?php echo site_url('auth/addAccount');?>" method="post">
    <legend>Kurti naują vartotojo paskyrą</legend>
  <fieldset>
    <div class="control-group">
        <label class="control-label" for="input01">El. Paštas</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="input01" name="email" value="<?php echo set_value('email');?>">
            <p class="help-block"><small>vardas@domenas.lt</small></p>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="input02">Slaptažodis</label>
        <div class="controls">
            <input type="password" class="input-xlarge" id="input02" name="password">
            <p class="help-block"><small>Min - 7 simboliai</small></p>
        </div>
    </div>

     <div class="control-group">
        <label class="control-label" for="input03">Patvirtinimas</label>
        <div class="controls">
            <input type="password" class="input-xlarge" id="input03" name="passwordConfirmation">
            <p class="help-block"><small>Dar kartą įveskite slaptažodį</small></p>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="input04">Vardas</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="input04" name="firstname">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="input05">Pavardė</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="input05" name="lastname">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="select01">Statusas</label>
        <div class="controls">
        <?php

        $options = array(
          "inactive"    => "Inactive",
          "active"    => "Active",
          "deleted"   => "Deleted"
        );
        
        echo form_dropdown("status", $options, '', 'id="select01"');

        ?>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="select02">Teisės</label>
        <div class="controls">
        <?php

        $options = array();
        foreach($ranks as $row){
          $options[$row->rid] = $row->fullname;
        }

        echo form_dropdown("rid", $options, '', 'id="select02"');

        ?>
        </div>
    </div>

    <div class="form-actions">
            <button type="submit" class="btn btn-primary">Registruoti</button>
          </div>

  </fieldset>
</form>
<?php if(!empty($error)):?>
<div class="alert alert-error">
    <a class="close" data-dismiss="alert">x</a>
    <h5 class="alert-heading">Klaida!</h5>
    <?php echo $error;?>
</div>
<?php endif;?>
</div>