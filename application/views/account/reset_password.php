<form class="form-horizontal" action="<?php echo $action;?>" method="post">
	<legend>Kurti naują vartotojo paskyrą</legend>
  	<fieldset>
		<div class="control-group">
	        <label class="control-label" for="input02">Slaptažodis</label>
	        <div class="controls">
	            <input type="password" class="input-xlarge" id="input02" name="password">
	            <p class="help-block"><small>Min - 7 simboliai</small></p>
	        </div>
	    </div>

	     <div class="control-group">
	        <label class="control-label" for="input03">Patvirtinimas</label>
	        <div class="controls">
	            <input type="password" class="input-xlarge" id="input03" name="passwordConfirmation">
	            <p class="help-block"><small>Dar kartą įveskite slaptažodį</small></p>
	        </div>
	    </div>
	
		<div class="form-actions">
	    	<button type="submit" class="btn btn-primary">Pakeisti slaptažodi</button>
	    </div>
	</fieldset>
</form>
<?php if(!empty($error)):?>
<div class="alert alert-error">
	<a class="close" data-dismiss="alert">x</a>
	<h5 class="alert-heading">Klaida!</h5>
	<?php echo $error;?>
</div>
<?php endif;?>
</div>