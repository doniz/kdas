<form class="form-vertical well" action="<?php echo site_url('main/password/email');?>" method="post">
	<fieldset>
		
		<div class="control-group">
			<label class="control-label" for="input01">El. Paštas</label>
			<div class="controls">
				<input type="text" class="input-xlarge" id="input01" name="email" value="<?php echo set_value('email');?>">
				<p class="help-block"><small>Įveskite savo el. pašto adresą</small></p>
			</div>
		</div>

		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Siųsti</button>
			<a class="btn" href="<?php echo site_url('main');?>">Atšaukti</a>
		</div>

	</fieldset>
</form>	
<?php
if(!empty($error)) { ?>
<div class="alert alert-error">
	<a class="close" data-dismiss="alert">x</a>
	<h5 class="alert-heading">Klaida!</h5>
	<?php echo $error;?>
</div>
<?php } ?>
</div>