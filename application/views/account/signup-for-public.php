<form class="form-horizontal well" action="<?php echo site_url('accounts/signup');?>" method="post">
    <legend>Kurti naują vartotojo paskyrą</legend>
  <fieldset>
  	<div style="float:left">
    <div class="control-group">
        <label class="control-label" for="input01">El. Paštas</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="input01" name="email" value="<?php echo set_value('email');?>">
            <p class="help-block"><small>vardas@domenas.lt</small></p>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="input02">Slaptažodis</label>
        <div class="controls">
            <input type="password" class="input-xlarge" id="input02" name="password">
            <p class="help-block"><small>Min - 7 simboliai</small></p>
        </div>
    </div>

     <div class="control-group">
        <label class="control-label" for="input03">Patvirtinimas</label>
        <div class="controls">
            <input type="password" class="input-xlarge" id="input03" name="passwordConfirmation">
            <p class="help-block"><small>Dar kartą įveskite slaptažodį</small></p>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="input04">Vardas</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="input04" name="firstname">
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="input05">Pavardė</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="input05" name="lastname">
        </div>
    </div>

    </div>
    <div style="float:right">
	
	    <div class="control-group">
	        <label class="control-label" for="input06">Telefonas</label>
	        <div class="controls">
	            <input type="text" class="input-xlarge" id="input06" name="phone">
	        </div>
	    </div>

	    <div class="control-group">
	        <label class="control-label" for="input08">Adresas</label>
	        <div class="controls">
	            <input type="text" class="input-xlarge" id="input08" name="address">
	        </div>
	    </div>
    
    </div>

  </fieldset>
  	<div class="form-actions" style="text-align:right">
		<button type="submit" class="btn btn-primary">Registruoti</button>
	</div>
</form>
<?php if(!empty($error)):?>
<div class="alert alert-error">
    <a class="close" data-dismiss="alert">x</a>
    <h5 class="alert-heading">Klaida!</h5>
    <?php echo $error;?>
</div>
<?php endif;?>
</div>