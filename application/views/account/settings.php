<h4>Nustatymai</h4>
<hr>
<div class="well">
<table class="table table-striped table-bordered">
  <tr>
    <th></th>
    <th colspan="2" style="text-align:center">Registracijos diena<br/>Nuo / Iki</th>
    <th colspan="5" style="text-align:center">Kita informacija</th>
  </tr>
  <tr>
    <td>#</td>
    <td style="text-align:center">Pradžios diena</td>
    <td style="text-align:center">Pabaigos diena</td>
    <td style="text-align:center">Raktas</td>
    <td style="text-align:center">Registruoti Vartotojai</td>
    <td style="text-align:center">Teisės</td>
    <td style="text-align:center">Uždaryta?</td>
	<td style="text-align:center">Veiksmai</td>
  </tr>
  <?php if(isset($settings)):?>
  <?php foreach($settings as $s):?>
  <tr>
    <td><?php echo $s->id;?></td>
    <td style="text-align:center"><?php echo date("Y.m.d", $s->start_date);?></td>
    <td style="text-align:center"><?php $end_date = date("Y.m.d", $s->end_date); echo $end_date;?></td>
    <td style="text-align:center"><?php echo $s->key;?></td>
    <td style="text-align:center"><?php echo $s->registrated_members;?></td>
    <td style="text-align:center"><?php $r = $this->permission->get_rank(array('rid' => $s->rid)); echo $r->fullname;?></td>
    <td style="text-align:center"><?php if($end_date < date("Y.m.d", time())) echo "Taip"; else echo "Ne";?></td>
	<td style="text-align:center"><a href="<?php echo site_url('accounts/settings/remove/' . $s->id);?>" class="btn btn-small" style="cursor:pointer" onClick="return confirm('Ar tikrai norite ištrinti?')"><i class="icon-remove"></i> Trinti</a></td>
  </tr>
  <?php endforeach;?>
  <?php endif;?>
</table>
</div>
<h4>Pridėkite leidimą registruotis</h4>
<hr>
<form name="add" action="<?php echo site_url('accounts/settings/add');?>" method="post" class="form-horizontal">
  <fieldset>

    <div class="control-group">
      <label class="control-label" for="input01">Diena kada uždaryti registracija</label>
      <div class="controls">
        <div class="input-append">
          <input type="text" class="input-small" id="input01" name="end_date" />
          <span class="add-on" style="cursor:pointer"><i class="icon-calendar"></i></span>
        </div>
      </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="select02">Teisės</label>
        <div class="controls">
        <?php

        $options = array();
        foreach($ranks as $row){
          if($row->rid != 5)
            $options[$row->rid] = $row->fullname;
        }

        echo form_dropdown("rid", $options, '', 'id="select02"');

        ?>
        <p class="help-block"><br/>Suteikti parinktas teises<br/>registruotam vartotojui</p>
        </div>
    </div>

    <div class="form-actions">
      <button type="submit" class="btn btn-primary">Pridėti</button>
    </div>

  </fieldset>
</form>
<?php if(!empty($error)):?>
<div class="alert alert-error">
    <a class="close" data-dismiss="alert">x</a>
    <h5 class="alert-heading">Klaida!</h5>
    <?php echo $error;?>
</div>
<?php endif;?>
<script type="text/javascript">
 $(function() {
    $('div.input-append span.add-on').on('click', function(){
      $( "#input01" ).focus();
    });

    $( "#input01" ).datepicker({
        minDate: +1,
        maxDate: "1M",
        dateFormat: "yy-mm-dd",
        showAnim: "fadeIn"
    });

  });
</script>