<?php

class Job_model extends CI_Model {

	function _required($required, $data){
	
		foreach($required as $field){
	
			if(!isset($data[$field])) return false;
	
			return true;
	
		}
	
	}

	function _default($defaults, $options){
	
		return array_merge($defaults, $options);
	
	}

	function secure($url = '') {

		$require_secure = false;

		if( !empty($url) ) {

			$permission = $this->permission->get(array('url' => $this->permission->_uri($url)));

			foreach($permission as $perm){

				if( $this->account->secure(array('rid' => $perm->rid)) )
					$require_secure = true;

			}


		}

		return $require_secure;

	}

	public function _not_exist_students($students){

		$not_exist_students = array();

		foreach($students as $student){

			if( !$this->get(array('student_id' => $student->uid)) )
				$not_exist_students[] = $student;

		}

		return $not_exist_students;
		
	}

	public function _not_exist_lectures($lectures){

		$not_exist_lectures = array();

		foreach($lectures as $lecture){

			if( !$this->get(array('lecture_id' => $lecture->uid)) )
				$not_exist_lectures[] = $lecture;

		}

		return $not_exist_lectures;

	}

	public function add($options = array()){

		if( !$this->_required(
			array(
				"lecture_id", "amount"
			),
		$options)
		) return false;

		$options = $this->_default(
			array("closed" => "false"),
			$options);


		$this->db->insert('subjects_amount', $options);

		return $this->db->insert_id();

	}

	public function update($options = array()){

		if( !$this->_required(
			array(
				"sa_id"
			),
		$options)
		) return false;

		$this->db->where("sa_id", $options["sa_id"]);
		$this->db->update('subjects_amount', $options);

		return ( $this->db->affected_rows() > 0 );

	}

	function delete($options = array()){
		if( !$this->_required(
			array('sa_id'),
			$options)
		) return false;

		$this->db->delete('subjects_amount', array('sa_id' => $options['sa_id']));

		return ( $this->db->affected_rows() > 0 );
	}

	function get($options = array(), $all = FALSE){

		if( isset($options['sa_id']) )
			$this->db->where('sa_id', $options['sa_id']);

		if( isset($options['lecture_id']) )
			$this->db->where('lecture_id', $options['lecture_id']);

		if( isset($options['amount']) )
			$this->db->where('amount', $options['amount']);

		if( $all == FALSE )
			$this->db->where('closed', 'false');

		$query = $this->db->get('subjects_amount');

		if( isset($options['sa_id']) || (isset($options['lecture_id']) && $all == FALSE) )
			return $query->row(0);

		return $query->result();

	}

}