<?php

class Permission extends CI_Model {

	public function __construct(){
		parent::__construct();

		$this->load->model('account');

	}

	function _required($required, $data){
		foreach($required as $field){
			if(!isset($data[$field])) return false;
			return true;
		}
	}
	function _default($defaults, $options){
		return array_merge($defaults, $options);
	}

	function _uri($url){

		if( !empty($url) ) {

			$uri = explode('/', $url);

			if( count($uri) >= 3 ) {

				$url = $uri[0] . '/' . $uri[1];

				return $url;

			}

			if( count($uri == 2) ) {

				if( !$this->get(array('url' => $url)) )
					$url = $uri[0];
				
			}

		}

		return $url;

	}

	public function get_rank($options = array()){

		if( isset($options['rid']) )
			$this->db->where('rid', $options['rid']);

		if( isset($options['name']) )
			$this->db->where('name', $options['name']);

		if( isset($options['fullname']) )
			$this->db->where('fullname', $options['fullname']);

		$query = $this->db->get('rank');

		if( isset($options['rid']) )
			return $query->row(0);

		return $query->result();

	}

	public function update($options = array()){
		if( !$this->_required(
			array(
				"pid"
			),
		$options)
		) return false;

		if( isset($options['rid']) )
			$options['rid'] = implode(',', $options['rid']);

		$this->db->where("pid", $options["pid"]);
		$this->db->update('permissions', $options);

		return ( $this->db->affected_rows() > 0 );

	}

	public function delete($id){
		
		$this->db->delete('permissions', array('pid' => $id));
		return ( $this->db->affected_rows() > 0 );

	}

	public function get($options = array()) {

		$option_list = array(
			'pid', 'rid', 'url'
		);

		foreach($option_list as $opt){

			if( isset($options[$opt]) )
				$this->db->where($opt, $options[$opt]);

		}

		// Limit & Offset
		if( isset($options['limit']) && isset($options['offset']) )

			$this->db->limit($options['limit'], $options['offset']);

		else if( isset($options['limit']) )

			$this->db->limit($options['limit']);

		// Sort
		if( isset($options['sort_by']) && isset($options['sort_direction']) )

			$this->db->order_by($options['sort_by'], $options['sort_direction']);

		$query = $this->db->get('permissions');

		if( isset($options['count']) )
			return $query->num_rows();

		if( isset($options['pid']) )
			return $query->row(0);

		return $query->result();

	}

	public function get_where_id($id){
		
		$permission = $this->get();
		$perm 		= array();

		if( !empty($id) ) {

			foreach($permission as $p) {

				$rid = explode(',', $p->rid);

				$key = '';


				foreach($rid as $r){

					if($r == $id){

						$perm[] = $p;

					}

				}



			}

		}

		return $perm;

	}

	public function add($options = array()) {
		if( !$this->_required(
				array(
					'rid', 'url', 'category', 'link_name'
				),
			$options)
		) return false;

		if( isset($options['rid']) )
			$options['rid'] = implode(',', $options['rid']);

		$options = $this->_default(
				array(
					'category' => 'puslapiai', 'icon_name' => '',
					'sub_url' => ''
				),
			$options);

		$this->db->insert('permissions', $options);

		return $this->db->insert_id();

	}

	public function create_navigation($options = array()){
		if( !$this->_required(
				array(
					'email', 'rid', 'uid'
				),
			$options)
		) return false;

		$navigation = array();
		$nav_db		= $this->get_where_id($options['rid']);


		if( !function_exists('subcategory') ) {

			function subcategory($url, $category, $array, &$new_array){

				$return_array = array();

				$index = 0;

				foreach( $array as $name ) {
					
					if( $name->sub_url === $url && $name->category === $category ) {

							$return_array[] = array(
									"name"	=> htmlspecialchars('<i class="'.$name->icon_name.'"></i> '.$name->link_name, ENT_QUOTES),
									"url"	=> $name->url
								);

							unset($array[$index]);

					}

					$index++;

				}

				$new_array = $array;

				return $return_array;
			
			}

		}


		function super_unique($array){

			$dropdown = array();

			foreach($array as $key => $value){

				if( count($value) > 2 ) {

					foreach($value['drop'] as $drop){

						$dropdown[] = $drop;

					}
				}
			}

			$index = 0;

			foreach($array as $key => $value){

				if( count($value) === 2 ) {

					foreach($dropdown as $d){

						if( $value['url'] == $d['url'])
							unset($array[$index]);
					}

				}

				$index++;

			}

			return $array;

		}


		foreach($nav_db as $nav){

			// puslapiai
			if( $nav->category == 'puslapiai' ) {

				$subcategory = subcategory($nav->url, $nav->category, $nav_db, $new_array);

				$nav_db = $new_array;

				if( is_array($subcategory) && count($subcategory) > 0 )

					$navigation[$nav->category][] = array(
							"name"	=> htmlspecialchars('<i class="'.$nav->icon_name.'"></i> '.$nav->link_name, ENT_QUOTES),
							"url"	=> $nav->url,
							"drop"	=> $subcategory
						);

				else

					$navigation[$nav->category][] = array(
							"name"	=> htmlspecialchars('<i class="'.$nav->icon_name.'"></i> '.$nav->link_name, ENT_QUOTES),
							"url"	=> $nav->url
						);

			}

			// vartotojai
			if( $nav->category == 'vartotojai' ) {

				$subcategory = subcategory($nav->url, $nav->category, $nav_db, $new_array);

				$nav_db = $new_array;

				if( is_array($subcategory) && count($subcategory) > 0 )

					$navigation[$nav->category][] = array(
							"name"	=> htmlspecialchars('<i class="'.$nav->icon_name.'"></i> '.$nav->link_name, ENT_QUOTES),
							"url"	=> $nav->url,
							"drop"	=> $subcategory
						);

				else

					$navigation[$nav->category][] = array(
							"name"	=> htmlspecialchars('<i class="'.$nav->icon_name.'"></i> '.$nav->link_name, ENT_QUOTES),
							"url"	=> $nav->url
						);
			}
		}

			if( isset($navigation['puslapiai']))
				$navigation['puslapiai'] = super_unique($navigation['puslapiai']);

			if( isset($navigation['vartotojai']))
				$navigation['vartotojai'] = super_unique($navigation['vartotojai']);


			ksort($navigation);

		
		//$permission = $this->permission->get(array('url' => $this->permission->_uri(uri_string())));

		//if(!$this->account->secure(array('rid' => $permission[0]->rid)))
		//	redirect('main');

		return $navigation;

	}

}