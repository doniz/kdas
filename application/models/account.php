<?php

class Account extends CI_Model {
	public function __construct(){
		parent::__construct();

		$this->load->library('password');
	}

	function _required($required, $data){
		foreach($required as $field){
			if(!isset($data[$field])) return false;
			return true;
		}
	}
	function _default($defaults, $options){
		return array_merge($defaults, $options);
	}
	function _optional($optional, $data){
		foreach($optional as $field){
			if(array_key_exists($field, $data)) return true;
		}
		return false;
	}

	public function _refering_url(){

		return isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';

	}

	public function get($options = array()) {
		
		// Qualification
		if( isset($options['uid']) )
			$this->db->where('uid', $options['uid']);
				
		if( isset($options['email']) )
			$this->db->where('email', $options['email']);
		
		if( isset($options['username']) )
			$this->db->where('username', $options['username']);

		if( isset($options['firstname']) )
			$this->db->where('firstname', $options['firstname']);
		
		if( isset($options['lastname']) )
			$this->db->where('lastname', $options['lastname']);
		
		if( isset($options['status']) )
			$this->db->where('status', $options['status']);
			
		if( isset($options['rid']) )
			$this->db->where('rid', $options['rid']);

		if( isset($options['activate']) )
			$this->db->where('activate', $options['activate']);
			
		// Limit / Offset
		if( isset($options["limit"]) && isset($options["offset"]) )
			$this->db->limit($options["limit"], $options["offset"]);
		else if( isset($options["limit"]) )
			$this->db->limit($options["limit"]);
			
		// Sort
		if( isset($options["sort_by"]) && isset($options["sort_direction"]) )
			$this->db->order_by($options["sort_by"], $options["sort_direction"]);
		
		if( !isset($options['status']) )
			$this->db->where('status !=', 'deleted');
			
		$query = $this->db->get('members');
		
		if( isset($options["count"]) )
			return $query->num_rows();
		
		if( isset($options['uid']) || isset($options['email']) )
			return $query->row(0);
			
		return $query->result();
	}

	public function activate($code = ''){
		if ( !empty($code) ) {

			$this->db->where('activate', $code);
			$this->db->where('status !=', 'active');
			$this->db->update('members', array('status' => 'active'));

			
			return ( $this->db->affected_rows() == 1 );
			
		}

		return false;

	}

	public function exist($field, $value) {

		$this->db->where($field, $value);
		$this->db->get('members');

		return ( $this->db->affected_rows() == 1 );
	}

	public function make_username($username){
		if ( $this->exist('username', $username) ) {

			if ( preg_match('/\d/', $username) ) {

				$length = strlen($username) - 1;

				if(is_numeric($username[$length])) {

					$number = (int)$username[$length] + 1;

					$username[$length] = $number;

				}

				if ( $this->exist('username', $username) )
					return $this->make_username($username);

			} else {

				$username .= '1';

				if ( $this->exist('username', $username) )
					return $this->make_username($username);
			}


		}

		return $username;
	}

	public function update($options = array()) {

		if( !$this->_required(
				array('uid'),
			$options)
		) return false;

		if( isset($options['email']) )
			$this->db->set('email', $options['email']);

		if( isset($options['username']) )
			$this->db->set('username', $options['username']);

		if( isset($options['status']) )
			$this->db->set('status', $options['status']);

		if( isset($options['firstname']) )
			$this->db->set('firstname', $options['firstname']);

		if( isset($options['lastname']) )
			$this->db->set('lastname', $options['lastname']);

		if( isset($options['salt']) )
			$this->db->set('salt', $options['salt']);

		if( isset($options['activate']) )
			$this->db->set('activate', $options['activate']);

		if( isset($options['phone']) )
			$this->db->set('phone', $options['phone']);

		if( isset($options['address']) )
			$this->db->set('address', $options['address']);

		if( isset($options['password']) ) {

			$options['salt'] = $this->password->_generate_salt();
			$options['password'] =
				$this->password->generate_hash(
					$options['salt'].$options['password']
				);

			$this->db->set('salt', $options['salt']);
			$this->db->set('password', $options['password']);

		}

		if( isset($options['rid']) )
			$this->db->set('rid', $options['rid']);

		$this->db->where('uid', $options['uid']);

		$this->db->update('members');

		return $this->db->affected_rows();
	}

	public function add($options = array()){
		if(!$this->_required(
				array('email', 'firstname',
					  'lastname', 'password'),
				$options)
			) return false;

		$options['salt'] = $this->password->_generate_salt();
		if(isset($options['password']))
			$options['password'] =
				$this->password->generate_hash(
						$options['salt'].$options['password']
					);

		$username = $this->make_username(
			strtolower($options['firstname']).'.'.
			strtolower($options['lastname'])
		);

		$options = $this->_default(
				array('status' => 'inactive', 'rid' => '4',
					'activate' => $this->password->_generate_salt(30),
					'created'  => human_to_unix(unix_to_human(time())),
					'username' => $username
					),
				$options);
		
		$this->db->insert('members', $options);
		return $this->db->insert_id();
	}

	public function get_rank($id = '') {
		if( empty($id) ) {
			$query = $this->db->get('rank');
			return $query->result();
		} else {
			$this->db->where('rid', $id);
			$query = $this->db->get('rank');
			return $query->row(0);
		}
	}

	public function login($options = array()){
		if(!$this->_required(
				array('email', 'password', 'remember'),
			$options)
		) return false;

		$member = $this->get(array('email' => $options['email']));

		if (!$member) return false;

		if ( !$this->password->check_password(
				$member->password,
				$options['password'], $member->salt
			) && $member->status == ('inactive' || 'deleted')
		) return false;
			
		if ( $member->status == 'inactive' || $member->status == 'deleted' )
			return false;

		if( $options['remember'] ) {

			$cookie = array(
				'name'		=> 'syscookie',
				'value'		=> $this->encrypt->encode($options['email'].':'.$options['password']),
				'expire'	=> time() + ( 3600 * 24 * 365 )
			);

			set_cookie($cookie);

		}

		$this->session->set_userdata('account', $member->email);
		$this->session->set_userdata('rank', $member->rid);
		$this->session->set_userdata('uid', $member->uid);

		return true;
	}

	public function logout(){
		$session_data = array(
			'account'	=> $this->session->userdata('account'),
			'uid'		=> $this->session->userdata('uid'),
			'rank'		=> $this->session->userdata('rank')
		);

		if ( !empty($session_data['account']) &&
			 !empty($session_data['uid']) &&
			 !empty($session_data['rank']) ) {

			$this->session->unset_userdata($session_data);
			delete_cookie('syscookie');

			return true;
		}
		return false;
	}

	public function _is_logged($options = array()){
		if( !$this->_required(
				array('email', 'uid', 'rid'),
			$options)
		) return false;

		$member = $this->get(
				array(
						'email' => $options['email'],
						'rid'	=> $options['rid'],
						'uid'	=> $options['uid']
					)
		);

		// Neprisijunges
		if ( !$member ) return false;

		// Prisijunges
		return true;
	}

	public function secure($options = array()){
		if ( !$this->_required(
				array('rid'),
			$options)
		) return false;

		$member_rank = $this->session->userdata('rank');
		$options['rid'] = explode(',', $options['rid']);

		if ( is_array($options['rid']) ) {

			foreach($options['rid'] as $option_rank){

				if ( $option_rank == $member_rank ) return true;

			}

		} else {

			if ( $member_rank == $options['rid'] ) return true;

		}

		return false;

	}
}