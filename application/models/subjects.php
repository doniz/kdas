<?php

class Subjects extends CI_Model {

	function _required($required, $data){
	
		foreach($required as $field){
	
			if(!isset($data[$field])) return false;
	
			return true;
	
		}
	
	}

	function _default($defaults, $options){
	
		return array_merge($defaults, $options);
	
	}

	public function _not_exist_students($students){

		$not_exist_students = array();

		foreach($students as $student){

			if( !$this->get(array('student_id' => $student->uid)) )
				$not_exist_students[] = $student;

		}

		return $not_exist_students;
		
	}

	function delete($options = array()){
		if( !$this->_required(
			array('sid', 'lecture_id'),
			$options)
		) return false;

		$this->db->delete('subjects', array('sid' => $options['sid'], 'lecture_id' => $options['lecture_id']));

		return ( $this->db->affected_rows() > 0 );
	}

	function is_closed($subject, $amount){

		foreach($amount as $x){

			if($subject->sa_id == $x->sa_id && $x->closed == "true")

				return true;

		}

		return false;

	}

	function is_busy($subject){

		if($subject->busy == "true")
			return true;

		return false;
	}

	function get($options = array()){

		if( isset($options['lecture_id']) )
			$this->db->where('lecture_id', $options['lecture_id']);

		if( isset($options['busy']) )
			$this->db->where('busy', $options['busy']);

		if( isset($options['student_id']) )
			$this->db->where('student_id', $options['student_id']);

		if( isset($options['sid']) )
			$this->db->where('sid', $options['sid']);

		if( isset($options['sa_id']) )
			$this->db->where('sa_id', $options['sa_id']);

		// Sort
		if( isset($options["sort_by"]) && isset($options["sort_direction"]) )
			$this->db->order_by($options["sort_by"], $options["sort_direction"]);

		$query = $this->db->get('subjects');

		if( isset($options['sid']) )
			return $query->row(0);

		return $query->result();

	}

	function update($options = array()){

		if( !$this->_required(
			array('sid'),
			$options)
		) return false;

		$options = $this->_default(
			array(
				"date"		 => human_to_unix(unix_to_human(time()))),
			$options);

		$this->db->where("sid", $options["sid"]);
		$this->db->update('subjects', $options);

		return ( $this->db->affected_rows() > 0 );

	}

	function insert($options = array()){

		if( !$this->_required(
			array('title', 'description', 'lecture_id', 'student_empty', 'sa_id'),
			$options)
		) return false;

		if( empty($options['student_empty']))
			$options['student_id'] = 0;

		// istrinti elementa / rakta is masyvo
		unset($options['student_empty']);

		$options = $this->_default(
			array(
				"student_id" => $options['student_id'],
				"busy"		 => 'false',
				"date"		 => human_to_unix(unix_to_human(time()))),
			$options);

		$this->db->insert('subjects', $options);

		return $this->db->insert_id();

	}

	function get_amount($options = array(), $all = FALSE){

		if( isset($options['sa_id']) )
			$this->db->where('sa_id', $options['sa_id']);

		if( isset($options['lecture_id']) )
			$this->db->where('lecture_id', $options['lecture_id']);

		if( isset($options['amount']) )
			$this->db->where('amount', $options['amount']);

		if( $all == FALSE )
			$this->db->where('closed', 'false');

		$query = $this->db->get('subjects_amount');

		if( isset($options['sa_id']) || (isset($options['lecture_id']) && $all == FALSE) )
			return $query->row(0);

		return $query->result();

	}
}