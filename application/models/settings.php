<?php

class Settings extends CI_Model {

	function _required($required, $data){
		foreach($required as $field){
			if(!isset($data[$field])) return false;
			return true;
		}
	}
	function _default($defaults, $options){
		return array_merge($defaults, $options);
	}

	public function get($options = array()){

		if( isset($options['id']))
			$this->db->where('id', $options['id']);

		if( isset($options['start_date']))
			$this->db->where('start_date', $options['start_date']);

		if( isset($options['end_date']))
			$this->db->where('end_date', $options['end_date']);

		if( isset($options['key']))
			$this->db->where('key', $options['key']);

		if( isset($options['registrated_members']))
			$this->db->where('registrated_members', $options['registrated_members']);

		if( isset($options['rid']))
			$this->db->where('rid', $options['rid']);

		$query = $this->db->get('members_settings');

		if( isset($options['id']) )
			return $query->row(0);

		if( isset($options['key']))
			return $query->row(0);

		return $query->result();

	}

	public function update($options = array()){
		if( !$this->_required(
			array('id'),
			$options)
		) return false;


		$this->db->where("id", $options["id"]);
		$this->db->update('members_settings', $options);

		return ( $this->db->affected_rows() > 0 );
	}

	public function is_match_key($key){
		if( !empty($key)){

			$result = $this->get(array('key' => $key));

			if(isset($result) && count($result) > 0){

				$current_date = date('Y-m-d h:i:s', time());
				$end_date 	  = date('Y-m-d h:i:s', $result->end_date);

				if($end_date < $current_date)
					return 2; // baigesi registracijos laikas
				else
					return 1; // registruotis galima
			}

			return 3; // nera sukurto registracijos leidimo
		}

		return 4; // neivestas raktas
	}
	
	public function delete($options = array()){
		if( !$this->_required(
			array('id'),
			$options)
		) return false;

		$this->db->delete('members_settings', array('id' => $options['id']));

		return ( $this->db->affected_rows() > 0 );
	}

	public function insert($options = array()){

		if( !$this->_required(
			array('start_date', 'end_date', 'key', 'rid'),
			$options)
		) return false;

		$options = $this->_default(
			array('registrated_members' => 0),
			$options);

		$this->db->insert('members_settings', $options);

		return $this->db->insert_id();
	}
}